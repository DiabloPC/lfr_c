<?php

$forum_loader->add_js($base_url. '/style/Oxygen/responsive-nav.min.js', array('weight' => 55, 'async' => false, 'group' => FORUM_JS_GROUP_SYSTEM));
$forum_loader->add_css($base_url.'/style/Oxygen/Oxygen.min.css', array('type' => 'url', 'group' => FORUM_CSS_GROUP_SYSTEM, 'media' => 'screen'));
$forum_loader->add_css($base_url.'/style/Oxygen/mobile.css', array('type' => 'url', 'group' => FORUM_CSS_GROUP_SYSTEM, 'media' => 'screen'));
echo '<link rel="stylesheet" type="text/css" media="screen" href="' . $base_url . '/style/Oxygen/Oxygen.min.css" />';
echo '<link rel="stylesheet/less" type="text/css" href="' . $base_url . '/style/Oxygen/css/default.less">';
echo '<script src="' . $base_url . '/style/Oxygen/js/hammer.min.js"></script>';
echo '<script src="' . $base_url . '/style/Oxygen/js/touch-menu-la.js"></script>';
echo '<script src="' . $base_url . '/style/Oxygen/js/less.min.js"></script>';


/*switch (date('dm')) {
	case '0104':
		$forum_loader->add_css($base_url.'/style/Oxygen/41.css', array('type' => 'url', 'group' => FORUM_CSS_GROUP_SYSTEM, 'media' => 'screen'));
		break;
}*/

// С учётом часового пояса, если юзер залогинен
if (format_time(time(), 1, 'dm', '', true) == '0104') {
		$forum_loader->add_css($base_url.'/style/Oxygen/41.css', array('type' => 'url', 'group' => FORUM_CSS_GROUP_SYSTEM, 'media' => 'screen'));
}

$tpl_main = str_replace('<!-- forum_board_title -->', forum_htmlencode($forum_config['o_board_title']), $tpl_main);
$tpl_main = str_replace('<!-- forum_lang_menu_admin -->', $lang_common['Menu admin'], $tpl_main);
$tpl_main = str_replace('<!-- forum_lang_menu_profile -->', $lang_common['Menu profile'], $tpl_main);

?>
