<?php
define('PUN_ROOT', './');
define('FORUM_ROOT', './');
require PUN_ROOT.'include/common.php';


if ($pun_user['g_read_board'] == '0')
    message($lang_common['No view']);


// Load the index.php language file
require FORUM_ROOT.'lang/'.$forum_user['language'].'/index.php';

define('PUN_ALLOW_INDEX', 1);
define('TOPICS_PER_PAGE', 100);
require FORUM_ROOT.'header.php';

$fid_list = array();
$categories = array();
$forums = array();

// get available forum list
$query = array(
	'SELECT'	=> 'c.id AS cid, c.cat_name, f.id AS fid, f.forum_name, f.num_posts, f.last_post, f.last_post_id, f.last_poster',
	'FROM'		=> 'categories AS c',
	'JOINS'		=> array(
		array(
			'INNER JOIN'	=> 'forums AS f',
			'ON'			=> 'c.id=f.cat_id'
		),
		array(
			'LEFT JOIN'		=> 'forum_perms AS fp',
			'ON'			=> '(fp.forum_id=f.id AND fp.group_id='.$forum_user['g_id'].')'
		)
	),
	'WHERE'		=> 'fp.read_forum IS NULL OR fp.read_forum=1',
	'ORDER BY'	=> 'c.disp_position, c.id, f.disp_position'
);

$result = $forum_db->query_build($query);
while ($cur_forum = $forum_db->fetch_assoc($result))
{
    $fid_list[] = $cur_forum['fid'];
    $forums[$cur_forum['fid']] = $cur_forum['forum_name'];
	$categories[$cur_category['cid']] = $cur_forum['cat_name'];
}

$fid_list = implode(',', $fid_list);

// get number of topics and which we have to start from
$result = $forum_db->query('SELECT count(*) FROM topics AS t INNER JOIN forums AS f ON f.id = t.forum_id WHERE f.id in ('.$fid_list.')');
$num_rows = $forum_db->fetch_row($result);
$num_rows = $num_rows[0];
$num_pages = ceil(($num_rows + 1) / TOPICS_PER_PAGE);
$p = (!isset($_GET['p']) || $_GET['p'] <= 1 || $_GET['p'] > $num_pages) ? 1 : $_GET['p'];
$start_from = TOPICS_PER_PAGE * ($p - 1);

// Generate paging links
$paging_links = $lang_common['Pages'].': '.paginate($num_pages, $p, 'map.php?', ' ');

// loop through topics
$result = $forum_db->query('SELECT f.cat_id, t.forum_id, t.id, t.subject, t.last_post, t.poster FROM
    '.$forum_db->prefix.'topics AS t INNER JOIN
    '.$forum_db->prefix.'forums AS f ON f.id = t.forum_id INNER JOIN
    '.$forum_db->prefix.'categories AS c ON f.cat_id = c.id
    WHERE f.id in ('.$fid_list.')
    ORDER BY c.disp_position, f.disp_position, f.cat_id, t.forum_id, t.last_post desc
    LIMIT '.$start_from.','.TOPICS_PER_PAGE);


$cur_category = 0;
$cur_forum = 0;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="SHORTCUT ICON" href="http://linuxforum.ru/favicon.ico" />

<meta name="description" content="LinuxForum  :: Форум о Linux" />
<title>LinuxForum  :: Форум о Linux</title>
<link rel="alternate" type="application/rss+xml" href="http://linuxforum.ru/extern.php?action=feed&amp;type=rss" title="RSS" />
<link rel="alternate" type="application/atom+xml" href="http://linuxforum.ru/extern.php?action=feed&amp;type=atom" title="ATOM" />
<link rel="stylesheet" type="text/css" media="screen" href="http://linuxforum.ru/style/Oxygen/Oxygen.css" />
<link rel="stylesheet" type="text/css" media="screen" href="http://linuxforum.ru/style/Oxygen/Oxygen_cs.css" />

</head>
<body>

<div id="brd-wrap" class="brd">
<div id="brd-index" class="brd-page basic-page">

<div class="linkst">
	<div class="inbox">
		<p class="pagelink conl"><?php echo $paging_links ?></p>
		<ul><li><a href="index.php"><?php echo $lang_common['Index'] ?></a></li><li>&nbsp;&raquo;&nbsp;LinuxForum</li></ul>
		<div class="clearer"></div>
	</div>
</div>
<div class="box">

	<style>
		#map .cat {PADDING-LEFT: 30px; PADDING-TOP: 20px; PADDING-BOTTOM: 2px; BACKGROUND: url(img/map/folder.gif) no-repeat 10px 2px; FONT-WEIGHT: bold}
		#map .frm {PADDING-LEFT: 46px; PADDING-TOP: 3px; PADDING-BOTTOM: 2px; BACKGROUND: url(img/map/folder.gif) no-repeat 26px 2px; FONT-WEIGHT: bold}
		#map .tpc {PADDING-LEFT: 62px; BACKGROUND: url(img/map/doc.gif) no-repeat 42px 2px}
	</style>

	<div class="inbox" id="map">
<?php

$doc_ico = '<img src="img/map/doc.gif" width="16" height="16">';
$folder_ico = '<img src="img/map/folder.gif" width="16" height="16">';
while ($cur_topic = $forum_db->fetch_assoc($result))
{
    //if ($cur_topic['cat_id'] != $cur_category)    // A new category since last iteration?
    //{
    //    echo "\t\t".'<div class="cat">'.htmlspecialchars($categories[$cur_category['cat_id']]).'</div>'."\n";
    //    $cur_forum = $cur_topic['cat_id'];
    //}

    if ($cur_topic['forum_id'] != $cur_forum)    // A new forum since last iteration?
    {
        echo "\t\t".'<div class="frm"><a href="viewforum.php?id='.$cur_topic['forum_id'].'">'.htmlspecialchars($forums[$cur_topic['forum_id']]).'</a></div>'."\n";
        $cur_forum = $cur_topic['forum_id'];
    }

    echo "\t\t".'<div class="tpc"><a href="viewtopic.php?id='.$cur_topic['id'].'">'.htmlspecialchars($cur_topic['subject']).'</a><br /> '.
        htmlspecialchars($cur_topic['poster'])."</div>\n";

}

?>
	<br />
	</div>
</div>
<br />
<div class="clearer"></div>
<div class="linkst">
	<div class="inbox">
		<p class="pagelink conl"><?php echo $paging_links ?></p>
		<ul><li><a href="index.php"><?php echo $lang_common['Index'] ?></a></li><li>&nbsp;&raquo;&nbsp;LinuxForum</li></ul>
		<div class="clearer"></div>
	</div>
</div>
</div>
</div>

</body>
</html>
