<?php

$lang_portal = array(

'Author'					=> 'Автор',
'Posted'					=> 'Опубликовано',
'Views'						=> 'Просмотров',
'Comments'					=> 'Комментариев',
'News'						=> 'Новости',
'Pages'						=> 'Страницы',
'No news'					=> 'Нет новостей',
'No pages'					=> 'Нет страниц',
'Read more'					=> 'Читать далее',
'Page does not exist'		=> 'Страница не существуетt',

// Admin panels head
'Panels'					=> 'Панели',

// Admin pages head
'Pages'						=> 'Страницы',

// Admmin portal head
'Portal'					=> 'Портал',

// Panels
// Who's online
'Welcome'					=> 'Добро пожаловать',
'Welcome Guest'				=> 'Добро пожаловать, гость.',
'Please login'				=> 'Пожалуйста войдите.',
'Not registered yet'		=> 'Не заренистрированы?',
'Forgotten your password'	=> 'Забыли пароль?',
'Online'					=> 'Онлайн:',
'Users'						=> 'Пользователей',
'Topics'					=> 'Тем',
'Posts'						=> 'Сообщений',
'Users online'				=> 'Пользователей онлайн',
'Guests online'				=> 'Гостей онлайн',

// Recent posts
'No posts' 					=> 'Нет сообщений',
'By'						=> 'от',

// Search
'Submit search'				=> 'Поиск',

);