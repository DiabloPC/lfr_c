<?php
/**
 * Allows users to search the forum based on various criteria.
 *
 * @copyright (C) 2008-2012 PunBB, partially based on code (C) 2008-2009 FluxBB.org
 * @license http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 * @package PunBB
 */

if (!defined('FORUM_ROOT')) {
	define('FORUM_ROOT', './');
}

require FORUM_ROOT . 'include/common.php';

($hook = get_hook('se_start')) ? eval($hook) : null;

// Load the search.php language file
require FORUM_ROOT . 'lang/' . $forum_user['language'] . '/search.php';

if ($forum_user['g_read_board'] == '0') {
	message($lang_common['No view']);
} elseif ($forum_user['g_search'] == '0') {
	message($lang_search['No search permission']);
}

($hook = get_hook('se_pre_search_query')) ? eval($hook) : null;

$forum_page['main_title'] = "Поиск с использованием внешних систем";

$forum_page['crumbs'] = array(
	array($forum_config['o_board_title'], forum_link($forum_url['index'])),
	$lang_common['Search']
);

if (isset($_POST['srch_submit'])) {
	switch ($_POST['srch_sys']) {
		case "Yandex":
			$srch_url = "https://yandex.ru/search/?text=" . $_POST['srch_txt'] . "%20site%3Alinuxforum.ru";
			break;
		case "Google":
			$srch_url = "https://www.google.ru/search?newwindow=1&q=" . $_POST['srch_txt'] . "+site%3Alinuxforum.ru";
			break;
	}
	redirect($srch_url, "");
}

require FORUM_ROOT . 'header.php';
ob_start();
?>
<div class="main-head">
	<h2 class="hn"><?php echo $lang_search['Search heading ext'] ?></h2>
</div>
<div class="main-content">
	<form id="fm_search_ext" method="post" class="frm-form" accept-charset="utf-8" action="<?php echo forum_link('search_ext.php') ?>">
		<input type="hidden" name="csrf_token" value="<?php echo generate_form_token(forum_link('search_ext.php')) ?>" />
		<input type="text" id="srch_txt" name="srch_txt" required="required" maxlength="64" value="<?php echo $lang_search['Search legend'] ?>" onfocus="onFocus()" onblur="onBlur()">
		<div id="second_line">
			<span for="srch_sys"><?php echo $lang_search['Search via'] ?></span>
			<div>
				<input type="radio" name="srch_sys" id="srch_sys" value="Yandex" required="required">&nbsp;Yandex<br />
				<input type="radio" name="srch_sys" id="srch_sys" value="Google" required="required">&nbsp;Google
			</div>
			<span class="submit primary"><input type="submit" name="srch_submit" id="srch_submit" value="<?php echo $lang_search['Submit search'] ?>"></span>
		</div>
	</form>
</div>
<script>
	function onFocus() {
		if (document.getElementById("srch_txt").value == '<?php echo $lang_search['Search legend'] ?>') {
			document.getElementById("srch_txt").value='';
			document.getElementById("srch_txt").style.color="#000";
		}
	}

	function onBlur() {
		if (document.getElementById("srch_txt").value == '') {
			document.getElementById("srch_txt").value='<?php echo $lang_search['Search legend'] ?>';
			document.getElementById("srch_txt").style.color="#aaa";
		}
	}
</script>
<?php
$tpl_temp = forum_trim(ob_get_contents());
$tpl_main = str_replace('<!-- forum_main -->', $tpl_temp, $tpl_main);
ob_end_clean();
require FORUM_ROOT . 'footer.php';

