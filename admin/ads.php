<?php
if (!defined('FORUM_ROOT'))
	define('FORUM_ROOT', '../');
define('FORUM_DISABLE_CSRF_CONFIRM', 'sdf');
require FORUM_ROOT.'include/common.php';

$permission=array(
    '2'=>array(1,2,3,4,5,6,7,8,9,10,11,12),
    '29559'=>array(1,2,5,6,8),
);

if(!is_array($permission[$forum_user['id']]))
    message($lang_common['No permission']);

$action = isset($_GET['action']) ? $_GET['action'] : 'edit_links';
if(isset($_GET['section'])) {
    if($_GET['section']=='about') {
        $section='about';
    } elseif($_GET['section']>=1 and $_GET['section']<=12) {
        $section=intval($_GET['section']);
        if(!in_array($section, $permission[$forum_user['id']]))
            message($lang_common['No permission']);
    }
}else{
    $section='about';
}


// Setup breadcrumbs
$forum_page['crumbs'] = array(
    array($forum_config['o_board_title'], forum_link($forum_url['index'])),
    'Реклама'
);

define('FORUM_PAGE', 'ads');
require FORUM_ROOT.'header.php';

// START SUBST - <!-- forum_main -->
ob_start();

?>
<div class="main-menu gen-content">
    <ul>
    <?php
        if(($section=='about')) {
            echo "<li class=\"first-item active\"><a href=\"/admin/ads.php?section=about\"><span>Главная</span></a></li>";
        } else {
            echo "<li class=\"first-item\"><a href=\"/admin/ads.php?section=about\"><span>Главная</span></a></li>";
        }
        if(is_array($permission[$forum_user['id']])) {
            $count=1;
            foreach($permission[$forum_user['id']] as $num) {
                if($section===$num) {
                    echo "<li class=\"active\"><a href=\"/admin/ads.php?section=$num\"><span>Место $count</span></a></li>";
                } else {
                    echo "<li><a href=\"/admin/ads.php?section=$num\"><span>Место $count</span></a></li>";
                }
                $count++;
            }
        }
        ?>
    </ul>
</div>
<div class="main-content main-frm">
<?php 
if(1<=$section and $section<=12) {
?>
<div class="admin-submenu gen-content">
    <ul>
<?php
    if($action=='add_links') {
?>
        <li class="active normal">
            <a href="/admin/ads.php?section=<?php echo $section ?>&action=add_links">Добавить ссылки</a>
        </li>
<?php
    } else {
?>
        <li class="first-item">
            <a href="/admin/ads.php?section=<?php echo $section ?>&action=add_links">Добавить ссылки</a>
        </li>
<?php
    }
    if($action=='edit_links') {
?>
        <li class="active normal">
            <a href="/admin/ads.php?section=<?php echo $section ?>&action=edit_links">Редактировать ссылки</a>
        </li>
<?php
    } else {
?>
        <li class="normal">
            <a href="/admin/ads.php?section=<?php echo $section ?>&action=edit_links">Редактировать ссылки</a>
        </li>
<?php
    }
?>
        <li class="normal">
    </ul>
</div>
<?php
}
?>
<div class="content-head">
<?php
if(1<=$section and $section<=12 and $action=='add_links') {
    if($_SERVER['REQUEST_METHOD']=='POST') {
        if(isset($_POST['submit_link'])) { // форма с добавлением одной ссылки
?>
<div class="main-content main-message">
<?php
$link=$_POST['link'];

$link = forum_htmlencode($link);
$link = preg_replace('/(.*?)&lt;a href=(?:&quot;|&\#039;)(.*?)(?:&quot;|&\#039;)&gt;(.*?)&lt;\/a&gt;(.*?)/i', '$1<a href=$2>$3</a>$4', $link);
$link = substr($link,0, 255); // длинна строки
$query = array(
    'INSERT'    => 'url, field_id',
    'INTO'      => 'ads',
    'VALUES'    => '\''.$forum_db->escape($link).'\', '.$section.''
);
$forum_db->query_build($query) or error(__FILE__, __LINE__);

echo "<b>Ваша ссылка \"</b>".$link."<b>\" добавлена!</b>";
         
?>
</div>
<?php
            
        }elseif(isset($_POST['submit_file'])){ // форма с добавлением файла
            if($_FILES['uploadfile']['error'] > 0)
            {
                //в зависимости от номера ошибки выводим соответствующее сообщение
                //UPLOAD_MAX_FILE_SIZE - значение установленное в php.ini
                //MAX_FILE_SIZE значение указанное в html-форме загрузки файла
                switch ($_FILES['uploadfile']['error']) {
                case 1: echo 'Размер файла превышает допустимое значение UPLOAD_MAX_FILE_SIZE'; break;
                case 2: echo 'Размер файла превышает допустимое значение MAX_FILE_SIZE'; break;
                case 3: echo 'Не удалось загрузить часть файла'; break;
                case 4: echo 'Файл не был загружен'; break;
                case 6: echo 'Отсутствует временная папка.'; break;
                case 7: echo 'Не удалось записать файл на диск.'; break;
                case 8: echo 'PHP-расширение остановило загрузку файла.'; break;
                }
                exit;
            }
            //проверяем MIME-тип файла
            if($_FILES['file_links']['type'] !== 'text/plain') {
                echo 'Вы пытаетесь загрузить не текстовый файл.';
                exit;
            }
            $blacklist = array(".php", ".phtml", ".php3", ".php4");
            foreach ($blacklist as $item) {
                if(preg_match("/$item\$/i", $_FILES['uploadfile']['name'])) {
                    echo "Нельзя загружать скрипты.";
                    exit;
                }
            }
            var_dump($_FILES['file_links']['tmp_name']);
        }
    }else{
?>
<fieldset class="frm-group group1">
    <form class="frm-form" action="/admin/ads.php?section=<?php echo $section ?>&action=add_links" accept-charset="utf-8" method="post">
        <div class="sf-set set">
            <div class="sf-box text">
                <label for="fld"><span><b>Введите ссылку</b></span></label><br>
                <span class="fld-input"><input type="text" size="80" value="<?php echo isset($_POST['link']) ? $_POST['link'] : '' ?>" name="link" id="fld"></span>
            </div>
        </div>
        <div class="frm-buttons">
            <span class="submit primary">
                <input type="submit" value="Добавить" name="submit_link">
            </span>
        </div>
    </form>
</fieldset>
<fieldset class="frm-group group2">
    <form class="frm-form" enctype="multipart/form-data" action="/admin/ads.php?section=<?php echo $section ?>&action=add_links" accept-charset="utf-8" method="post">
        <div class="sf-set set">
            <div class="sf-box text required">
                <label for="fld">
                    <span>Выберите файл</span>
                    <small>Затем нажмите кнопку загрузить</small>
                </label>
                <br>
                <input type="hidden" name="MAX_FILE_SIZE" value="500000">
                <span class="fld-input"><input id="fld1" type="file" size="82" name="file_links"></span>
            </div>
        </div>
        <div class="frm-buttons">
            <span class="submit primary"><input type="submit" value="Загрузить" name="submit_file"></span>
        </div>
</fieldset>
<?php
    }
}elseif(1<=$section and $section<=12 and $action=='edit_links') {
    $query = array(
        'SELECT' => 'url, field_id',
        'FROM' => 'ads',
        'WHERE' => 'field_id = '.$section
    );

    $result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
?>
<div class="ct-box data-box">
    <ul class="data-box">
<?php
    while ($ads = $forum_db->fetch_assoc($result)) {
?>
    <li>
<?php echo $ads['url'] ?>
    </li>
<?php        
    }
?>
    </ul>
</div>
<?php
}elseif($section=='about') 
{
?>
        <div class="main-content main-message">
<?php
    echo 'Рекламный модуль.';
?>
        </div>
<?php
}
?>
    </div>
</div>
<?php
$tpl_temp = forum_trim(ob_get_contents());
$tpl_main = str_replace('<!-- forum_main -->', $tpl_temp, $tpl_main);
ob_end_clean();
// END SUBST - <!-- forum_main -->

require FORUM_ROOT.'footer.php';
