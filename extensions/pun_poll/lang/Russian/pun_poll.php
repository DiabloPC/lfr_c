<?php

/**
 * Lang file for voting
 *
 * @copyright Copyright (C) 2008 PunBB
 * @license http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 * @package pun_poll
 */

if (!defined('FORUM')) exit;

$lang_pun_poll = array(
	'Poll question'					=>	'Вопрос об опросе',
	'Voting answer'					=>	'Выбор опроса',
	'Able revote'					=>	'Позволить пользователям изменять свои мнения.',

//page Administration -> Settings -> Features
	'Name plugin'					=>	'Параметры настройки для опросов',
	'Maximum answers'				=>	'Максимальные ответы в опросе (2-100).',
	'Disable revoting'				=>	'Позволить изменение голосования.',
	'Disable see results'			=>	'Позволить видеть результаты',
	'Maximum answers info'			=>	'Максимальные ответы',
	'Disable see results info'		=>	'Пользователи видят результаты опроса без голосования.',
	'Disable revoting info'			=>	'Повторно голосовать',
	'Poll add'						=>	'Разрешить пользователям использовать опросы в своих темах.',
	'Permission'					=>	'Голосование разрешения',

// general
	'Users count'					=>	'Голоса: ',
	'Preview poll'					=>	'Предварительный просмотр отредактированного опроса.',
	'Preview poll question'			=>	'<strong>%s</strong>',
	'Revote'						=>	'Повторный опрос',
	'Summary count'					=>	'Количество вариантов',
	'Allow days'					=>	'Опрос действует (дней)',
	'Votes needed'					=>	'Количество голосов',
	'Input error'					=>	'Вы должны ввести номер дней для голосования или количество голосов.',
	'Count'							=>	'Количество',
	'Button note'					=>	'Обновление опроса',
	'Show poll res'					=>	'Показ результатов опроса',
	'All ch vote'					=>	'Позволить изменять голос',
	'Max cnt options'				=>	'Количество ответов может быть больше, чем %s.',
	'Min cnt options'				=>	'Количество ответов может быть меньше чем 2.',
	'Days limit'					=>	'Количество дней опроса может быть больше чем 90 и меньше, чем 1.',
	'Votes count'					=>	'Количество	голосов может быть больше чем 500 и меньше чем 10.',
	'Header note'					=>	'Голосование',
	'Options'						=>	'Опции голосования',
	'Results'						=>	'Результаты голосования: ',
	'No votes'						=>	'Еще нет голосов.',
	'Dis read vote'					=>	'Вы можете видеть голоса, пока Вы не голосуете за себя.',
	'But note'						=>	'Голос',
	'User vote error'				=>	'Вы уже голосовали.',
	'End of vote'					=>	'Вы голосуете здесь, когда опрос уже закончен.',
	'Reset res'						=>	'Сброс результатов голосования',
	'Reset res notice'				=>	'Сброс результатов голосования.',
	'Remove v'						=>	'Удалить опрос',
	'Remove v notice'				=>	'Удалить опрос.',
	'Empty question'				=>	'Вы должны войти в вопрос опроса.',
	'Empty answers'					=>	'Вы должны войти в ответы опроса.',
	'Merge error'					=>	'Вы сливаете эти темы, потому что 2 или больше темы включают голосование. Удалите голосование перед слиянием.',
	'Edit voting'					=>	'Если Вы хотите отредактировать вопрос, или ответы голосования напишите электронное письмо администратору Форума <a href="mailto:%s">%s</a>.',
	'Empty option count'			=>	'Вы должны войти в количество вариантов опроса.',

	'Question len limit'			=>	'Длина вопроса не может быть меньше чем 5 символов.',

	'Maximum votes note'			=>	'Максимальное количество голосов в опросе',
	'Days voting note'				=>	'Количество дней для голосования',

	'Poll redirect'					=>	'Опрос добавлен.',
	'Days, votes count'				=>	'Кол-во дней и кол-во голосов',

	'Hide poll'						=> 'Скрыть блок опроса',
	'Show poll'						=> 'Показать блок опроса',
	'Add poll option'				=> 'Добавить опции к опросу',
);
