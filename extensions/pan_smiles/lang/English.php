<?php

if (!defined('FORUM')) die();

$lang_pan_smiles = array(
	'settings'				=> 'Settings',
	'settings_smilies'		=> 'Settings smilies bar',
	'location'				=> 'Location',
	'location_i'			=> 'Select location smilies',
	
	'location_top'			=> 'Show to top',
	'location_bottom'		=> 'Show to bottom',
	'location_left'			=> 'Show to left',
	
	'smile_pack'			=> 'Smile-pack',
	'smile_pack_i'			=> 'Select your smile-pack from list',
)

?>