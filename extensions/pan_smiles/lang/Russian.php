<?php

if (!defined('FORUM')) die();

$lang_pan_smiles = array(
	'settings'				=> 'Настройки',
	'settings_smilies'		=> 'Настройки блока смайликов',
	'location'				=> 'Расположение',
	'location_i'			=> 'Выберите расположение смайлов',
	
	'location_top'			=> 'Показывать над формой',
	'location_bottom'		=> 'Показывать под формой',
	'location_left'			=> 'Показывать слева от формы',
	
	'smile_pack'			=> 'Смайл-пак',
	'smile_pack_i'			=> 'Выберите ваш набор из списка смайл-паков',
	
	
)

?>