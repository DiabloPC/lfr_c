<?php

if (!defined('FORUM')) die();

if (file_exists($ext_info['path'].'/lang/'.$forum_user['language'].'.php'))
	require $ext_info['path'].'/lang/'.$forum_user['language'].'.php';
else
	require $ext_info['path'].'/lang/English/'.$ext_info['id'].'.php';

if ($ext['id'] == $ext_info['id'])
	$forum_page['ext_actions'][$ext_info['id']] = '<span><a href="'.forum_link($forum_url['admin_settings_features']).'#'.$ext_info['id'].'">'.$lang_pan_smiles['settings'].'</a></span>';

