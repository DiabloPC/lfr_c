<?php

if (!defined('FORUM')) die();

if ($forum_config['o_pan_smiles_location'] == '1')
{
	if (!isset($smiles_styles_loaded))
	{
		$smiles_styles_loaded = true;
	
	if ($forum_user['style'] != 'Oxygen' && file_exists($ext_info['url'].'/style/'.$forum_user['style'].'/pan_smiles_top.css'))
		$forum_loader->add_css($ext_info['url'].'/style/'.$forum_user['style'].'/pan_smiles_top.css', array('type' => 'url', 'weight' => '90', 'media' => 'screen'));
	else
		$forum_loader->add_css($ext_info['url'].'/style/Oxygen/pan_smiles_top.css', array('type' => 'url', 'weight' => '90', 'media' => 'screen'));
	}

	$forum_loader->add_js($ext_info['url'].'/js/pan_smiles.js', array('type' => 'url', 'group' => FORUM_JS_GROUP_COUNTER));

}
else if ($forum_config['o_pan_smiles_location'] == '2')
{
	if (!isset($smiles_styles_loaded))
	{
		$smiles_styles_loaded = true;
	
	if ($forum_user['style'] != 'Oxygen' && file_exists($ext_info['url'].'/style/'.$forum_user['style'].'/pan_smiles_bottom.css'))
		$forum_loader->add_css($ext_info['url'].'/style/'.$forum_user['style'].'/pan_smiles_bottom.css', array('type' => 'url', 'weight' => '90', 'media' => 'screen'));
	else
		$forum_loader->add_css($ext_info['url'].'/style/Oxygen/pan_smiles_bottom.css', array('type' => 'url', 'weight' => '90', 'media' => 'screen'));
	}
}
else if ($forum_config['o_pan_smiles_location'] == '3')
{
	if (!isset($smiles_styles_loaded))
	{
		$smiles_styles_loaded = true;
	
	if ($forum_user['style'] != 'Oxygen' && file_exists($ext_info['url'].'/style/'.$forum_user['style'].'/pan_smiles_left.css'))
		$forum_loader->add_css($ext_info['url'].'/style/'.$forum_user['style'].'/pan_smiles_left.css', array('type' => 'url', 'weight' => '90', 'media' => 'screen'));
	else
		$forum_loader->add_css($ext_info['url'].'/style/Oxygen/pan_smiles_left.css', array('type' => 'url', 'weight' => '90', 'media' => 'screen'));
	}
}


