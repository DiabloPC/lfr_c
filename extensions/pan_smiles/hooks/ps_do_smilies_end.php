<?php

if (!defined('FORUM')) die();

//Others Replacement
foreach ($smilies as $smiley_text => $smiley_img)
{
	if (forum_htmlencode(stripslashes($smiley_text)) != $smiley_text)
	{
		if (strpos($text, forum_htmlencode(stripslashes($smiley_text))) !== false)
		{
			$text = preg_replace("#(?<=[>\s])".preg_quote(forum_htmlencode(stripslashes($smiley_text)), '#')."(?=\W)#m", '<img src="'.$ext_info['url'].'/pack/'.$smiley_img.'" alt="'.substr($smiley_img, 0, strrpos($smiley_img, '.')).'" />', $text);
		}
	}
}


