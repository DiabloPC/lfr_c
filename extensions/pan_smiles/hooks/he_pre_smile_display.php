<?php

if (!defined('FORUM')) die();

foreach ($smilies as $smiley_text => $smiley_img)
	$smiley_groups[$smiley_img][] = $smiley_text;

foreach ($smiley_groups as $smiley_img => $smiley_texts)
	echo "\t\t\t\t".'<p>'.implode(' '.$lang_common['and'].' ', $smiley_texts).' <span>'.$lang_help['produces'].'</span> <img src="'.$ext_info['url'].'/pack/'.$smiley_img.'" alt="'.$smiley_texts[0].'" /></p>'."\n";

unset($smilies);
unset($smiley_groups);

$smiley_groups = $smilies = array();
