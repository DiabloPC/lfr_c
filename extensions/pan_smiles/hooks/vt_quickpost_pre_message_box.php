<?php

if (!defined('FORUM')) die();

if ($forum_config['o_pan_smiles_location'] == '1')
{
	echo '<div id="bbcode_smilies" style="display:none">';
	foreach (array_unique($smilies_pack) as $smile_text => $smile_file)
	{
		echo '<img onclick="PUNBB.pun_bbcode.insert_text(\' '.$smile_text.' \', \'\');showhide(\'bbcode_smilies\');" src="'.$ext_info['url'].'/pack/'.$smile_file.'" alt="'.$smile_text.'" />'."\n";
	}
	echo '</div>';
}
else if ($forum_config['o_pan_smiles_location'] == '3')
{
	echo '<div id="bbcode_smilies">';
	foreach (array_unique($smilies_pack) as $smile_text => $smile_file)
	{
		echo '<img onclick="PUNBB.pun_bbcode.insert_text(\' '.$smile_text.' \', \'\');" src="'.$ext_info['url'].'/pack/'.$smile_file.'" alt="'.$smile_text.'" />'."\n";
	}
	echo '</div>';
}
