<?php

if (!defined('FORUM')) die();

if (file_exists($ext_info['path'].'/lang/'.$forum_user['language'].'.php'))
	require $ext_info['path'].'/lang/'.$forum_user['language'].'.php';
else
	require $ext_info['path'].'/lang/English/'.$ext_info['id'].'.php';

//Get All Dirs
function pan_smiles_get_packs($relative_path)
{
	$arr_dirs = array();
	if ($handle = opendir($relative_path) )
	{
		while (false !== ($dirname = readdir($handle) ) )
		{
			$dirname = $relative_path.'/'.$dirname;
			$tempname = basename($dirname);

			if (is_dir($dirname) && ($tempname != '.') && ($tempname != '..') && ($tempname != 'Default') )
				$arr_dirs[] = $tempname;
		}
		closedir($handle);
	}
	return $arr_dirs;
}

$smile_pack = pan_smiles_get_packs($ext_info['path'].'/pack');

?>
			<div class="content-head" id="<?php echo $ext_info['id'] ?>">
				<h2 class="hn"><span><?php echo $lang_pan_smiles['settings_smilies'] ?></span></h2>
			</div>

			<div class="sf-set set<?php echo ++$forum_page['item_count'] ?>">
				<div class="sf-box select">
					<label for="fld"><span><?php echo $lang_pan_smiles['location'] ?></span><small><?php echo $lang_pan_smiles['location_i'] ?></small></label><br />
					<span class="fld-input"><select id="fld" name="form[pan_smiles_location]">
<?php

$smilies_location = array(
	'1'	=> $lang_pan_smiles['location_top'],
	'2'	=> $lang_pan_smiles['location_bottom'],
	'3'	=> $lang_pan_smiles['location_left'],
);

foreach ($smilies_location as $key => $val)
{
	if ($forum_config['o_pan_smiles_location'] == $key)
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$key.'" selected="selected">'.$val.'</option>'."\n";
	else
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$key.'">'.$val.'</option>'."\n";
}
?>
					</select></span>
				</div>
			</div>
			
			
			
			<div class="sf-set set<?php echo ++$forum_page['item_count'] ?>">
				<div class="sf-box select">
					<label for="fld"><span><?php echo $lang_pan_smiles['smile_pack'] ?></span><small><?php echo $lang_pan_smiles['smile_pack_i'] ?></small></label><br />
					<span class="fld-input"><select id="fld" name="form[pan_smiles_pack]">
<?php

foreach ($smile_pack as $key => $val)
{
	if ($forum_config['o_pan_smiles_pack'] == $val)
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$val.'" selected="selected">'.$val.'</option>'."\n";
	else
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$val.'">'.$val.'</option>'."\n";
}

?>
					</select></span>
				</div>
			</div>

<?php

