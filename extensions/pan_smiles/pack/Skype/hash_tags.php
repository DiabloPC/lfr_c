<?php

if (!defined('FORUM')) die();

$cur_dir = basename(dirname(__FILE__));

$smilies_pack = array(
	':grinsen:'			=> $cur_dir.'/grinsen.gif',
	':hihi:'			=> $cur_dir.'/hihi.gif',
	':zwinkern:'		=> $cur_dir.'/zwinkern.gif',
	':breites:'			=> $cur_dir.'/breites.gif',
	':drunk:'			=> $cur_dir.'/drunk.gif',
	':freches:'			=> $cur_dir.'/freches.gif',
	':mm:'				=> $cur_dir.'/mm.gif',
	':rock:'			=> $cur_dir.'/rock.gif',
	':sprachlos:'		=> $cur_dir.'/sprachlos.gif',
	':ueberrrascht:'	=> $cur_dir.'/ueberrrascht.gif',
	':verlegen:'		=> $cur_dir.'/verlegen.gif',
	':verwirrt:'		=> $cur_dir.'/verwirrt.gif',
	':weinen:'			=> $cur_dir.'/weinen.gif',
	':boese:'			=> $cur_dir.'/boese.gif',
	':cool:'			=> $cur_dir.'/cool.gif',
	':badit:'			=> $cur_dir.'/badit.gif',
	':finger:'			=> $cur_dir.'/finger.gif',
	':headbang:'		=> $cur_dir.'/headbang.gif',
	':puke:'			=> $cur_dir.'/puke.gif',
	':traurig:'			=> $cur_dir.'/traurig.gif',
	':)'				=> $cur_dir.'/grinsen.gif',
	':('				=> $cur_dir.'/traurig.gif',
	';)'				=> $cur_dir.'/zwinkern.gif',
);

