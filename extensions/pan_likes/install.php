<?php

if(!defined('PAN_INSTALL')) die();

if (!$forum_db->table_exists('pan_likes'))
{
	$schema = array(
		'FIELDS' => array(
			'id'			=> array(
				'datatype'		=> 'SERIAL',
				'allow_null'	=> false
			),
			'topic_id'		=> array(
				'datatype'		=> 'INT(10) UNSIGNED',
				'allow_null'	=> false,
				'default'		=> '0'
			),
			'post_id'		=> array(
				'datatype'		=> 'INT(10) UNSIGNED',
				'allow_null'	=> false,
				'default'		=> '0'
			),
			'user_id_to'	=> array(
				'datatype'		=> 'INT(10) UNSIGNED',
				'allow_null'	=> false,
				'default'		=> '0'
			),
			'user_id'		=> array(
				'datatype'		=> 'INT(10) UNSIGNED',
				'allow_null'	=> false,
				'default'		=> '0'
			),
			'user_ip'		=>	array(
				'datatype'		=> 'VARCHAR(39)',
				'allow_null'	=> false,
				'default'		=> '\'0.0.0.0\''
			),
			'liked'			=> array(
				'datatype'		=> 'INT(10) UNSIGNED',
				'allow_null'	=> true
			),
			'status'		=> array(
				'datatype'		=> 'TINYINT(1)',
				'allow_null'	=> false,
				'default'		=> '1'
			),
		),
		'PRIMARY KEY'	=> array('id'),
	);
	$forum_db->create_table('pan_likes', $schema);
}

if (!$forum_db->field_exists('users', 'pan_likes'))
	$forum_db->add_field('users', 'pan_likes', 'INT(10)', false, '0');

if (!$forum_db->field_exists('posts', 'pan_likes'))
	$forum_db->add_field('posts', 'pan_likes', 'INT(10)', false, '0');


if (!$forum_db->field_exists('groups', 'g_pan_likes'))
	$forum_db->add_field('groups', 'g_pan_likes', 'TINYINT(1)', true, '1');

$query = array(
	'UPDATE'	=> 'groups',
	'SET'		=> 'g_pan_likes=0',
	'WHERE'		=> 'g_id=2'
);
$forum_db->query_build($query) or error(__FILE__, __LINE__);

