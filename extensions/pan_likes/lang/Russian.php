<?php

$lang_pan_likes = array (
	//User Info block
	'user_likes_sum'					=> 'Спасибо: ',
	
	//Post Info
	'liked_for_post'					=> 'Это понравилось пользователям: %s',
	'no_one_liked'						=> 'Пока еще никому не понравилось.',
	'pan_likes_guests'					=> 'Вы входите в группу, пользователям которой не разрешено благодарить',
	'i_like_it'							=> 'Сказать Спасибо',
	'you_like_added'					=> 'Ваше спасибо добавлено в рейтинг',
	
	//Settings Likes in Groups
	'g_pan_likes_head'					=> 'Настройки участия в рейтинге',
	'g_pan_likes_title'					=> 'Разрешить',
	'g_pan_likes_desc'					=> 'Позволить этой группе пользователей благодарить',
	
);

