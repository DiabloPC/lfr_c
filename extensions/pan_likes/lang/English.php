<?php

$lang_pan_likes = array (
	//User Info block
	'user_likes_sum'					=> 'Likes: ',
	
	//Post Info
	'liked_for_post'					=> 'Users liked this: %s',
	'no_one_liked'						=> 'Nobody liked it yet.',
	'pan_likes_guests'					=> 'You are a member of a group whose users are not allowed to put their likes',
	'i_like_it'							=> 'I like it',
	'you_like_added'					=> 'Your favorite was added to the rating',
	
	//Settings Likes in Groups
	'g_pan_likes_head'					=> 'Options for ranking participation',
	'g_pan_likes_title'					=> 'Allow',
	'g_pan_likes_desc'					=> 'Allow this group of users to put their likes',
	
);
