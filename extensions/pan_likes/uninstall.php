<?php 

if(!defined('PAN_UNINSTALL')) die();

if ($forum_db->table_exists('pan_likes'))
	$forum_db->drop_table('pan_likes');

if ($forum_db->field_exists('users', 'pan_likes_enable'))
	$forum_db->drop_field('users', 'pan_likes_enable');

if ($forum_db->field_exists('users', 'pan_likes_disable_adm'))
	$forum_db->drop_field('users', 'pan_likes_disable_adm');

if ($forum_db->field_exists('users', 'pan_likes'))
	$forum_db->drop_field('users', 'pan_likes');

if ($forum_db->field_exists('users', 'pan_dislikes'))
	$forum_db->drop_field('users', 'pan_dislikes');


if ($forum_db->field_exists('posts', 'pan_likes'))
	$forum_db->drop_field('posts', 'pan_likes');

if ($forum_db->field_exists('posts', 'pan_dislikes'))
	$forum_db->drop_field('posts', 'pan_dislikes');


if ($forum_db->field_exists('groups', 'g_pan_likes'))
	$forum_db->drop_field('groups', 'g_pan_likes');

if ($forum_db->field_exists('groups', 'g_pan_likes_min'))
	$forum_db->drop_field('groups', 'g_pan_likes_min');

if ($forum_db->field_exists('groups', 'g_pan_likes_loss'))
	$forum_db->drop_field('groups', 'g_pan_likes_loss');

