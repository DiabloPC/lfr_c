<?php

if (!defined('FORUM')) die();

if (file_exists($ext_info['path'].'/lang/'.$forum_user['language'].'.php') )
	require $ext_info['path'].'/lang/'.$forum_user['language'].'.php';
else
	require $ext_info['path'].'/lang/English.php';
	
?>

				<div class="content-head">
					<h3 class="hn"><span><?php echo $lang_pan_likes['g_pan_likes_head'] ?></span></h3>
				</div>
			
				<div class="sf-set set<?php echo ++$forum_page['item_count'] ?>">
					<div class="sf-box checkbox">
						<input type="hidden" name="pan_likes" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="pan_likes" value="1"<?php if ($group['g_pan_likes'] == '1') echo ' checked="checked"' ?> /></span>
						<label for="fld<?php echo $forum_page['fld_count'] ?>"><span><?php echo $lang_pan_likes['g_pan_likes_title'] ?></span><?php echo $lang_pan_likes['g_pan_likes_desc'] ?></label><br />
					</div>
				</div>



