<?php

if (!defined('FORUM')) die();

$topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : 0;
$post_id = isset($_GET['post_id']) ? intval($_GET['post_id']) : 0;
$user_id_to = isset($_GET['user_id_to']) ? intval($_GET['user_id_to']) : 0;

if ($forum_user['g_pan_likes'] == '1' && ($post_id > 0) && ($topic_id > 0) && ($user_id_to > 0))
{
	if (file_exists($ext_info['path'].'/lang/'.$forum_user['language'].'.php') )
		require $ext_info['path'].'/lang/'.$forum_user['language'].'.php';
	else
		require $ext_info['path'].'/lang/English.php';
	
	$query = array(
		'SELECT'	=> 'post_id',
		'FROM'		=> 'pan_likes',
	);
	
	if ($forum_user['is_guest'])
		$query['WHERE'] = 'post_id='.$post_id.' AND user_ip=\''.$forum_db->escape(get_remote_address()).'\'';
	else
		$query['WHERE'] = 'post_id='.$post_id.' AND user_id='.$forum_user['id'];
		
	($hook = get_hook('pan_likes_mi_new_action_act_like_sel_likes_row')) ? eval($hook) : null;
	$result_likes = $forum_db->query_build($query) or error(__FILE__, __LINE__);
	
	if ($forum_user['is_guest'])
	{
		if ($forum_db->num_rows($result_likes) == 0)
		{
			$query = array(
				'INSERT'	=> 'topic_id, post_id, user_id_to, user_id, user_ip, liked',
				'INTO'		=> 'pan_likes',
				'VALUES'	=> ''.$topic_id.', '.$post_id.', '.$user_id_to.', '.$forum_user['id'].' ,\''.$forum_db->escape(get_remote_address()).'\', '.time()
			);
			//($hook = get_hook('pan_likes_mi_new_action_insert_like')) ? eval($hook) : null;
			$forum_db->query_build($query) or error(__FILE__, __LINE__);
			
			$query = array(
				'UPDATE'	=> 'users',
				'SET'		=> 'pan_likes=pan_likes+1',
				'WHERE'		=> 'id='.$user_id_to
			);
			//($hook = get_hook('pan_likes_mi_new_action_update_like_user')) ? eval($hook) : null;
			$forum_db->query_build($query) or error(__FILE__, __LINE__);
			
			$query = array(
				'UPDATE'	=> 'posts',
				'SET'		=> 'pan_likes=pan_likes+1',
				'WHERE'		=> 'id='.$post_id
			);
			//($hook = get_hook('pan_likes_mi_new_action_update_like_post')) ? eval($hook) : null;
			$forum_db->query_build($query) or error(__FILE__, __LINE__);
			
			$message = $lang_pan_likes['you_like_added'];
		}
		
	}
	else
	{
		if ($forum_db->num_rows($result_likes) == 0)
		{
			$query = array(
				'INSERT'	=> 'topic_id, post_id, user_id_to, user_id, user_ip, liked',
				'INTO'		=> 'pan_likes',
				'VALUES'	=> ''.$topic_id.', '.$post_id.', '.$user_id_to.', '.$forum_user['id'].' ,\''.$forum_db->escape(get_remote_address()).'\', '.time()
			);
			//($hook = get_hook('pan_likes_mi_new_action_insert_like')) ? eval($hook) : null;
			$forum_db->query_build($query) or error(__FILE__, __LINE__);
			
			$query = array(
				'UPDATE'	=> 'users',
				'SET'		=> 'pan_likes=pan_likes+1',
				'WHERE'		=> 'id='.$user_id_to
			);
			//($hook = get_hook('pan_likes_mi_new_action_update_like_user')) ? eval($hook) : null;
			$forum_db->query_build($query) or error(__FILE__, __LINE__);
			
			$query = array(
				'UPDATE'	=> 'posts',
				'SET'		=> 'pan_likes=pan_likes+1',
				'WHERE'		=> 'id='.$post_id
			);
			//($hook = get_hook('pan_likes_mi_new_action_update_like_post')) ? eval($hook) : null;
			$forum_db->query_build($query) or error(__FILE__, __LINE__);
			
			$message = $lang_pan_likes['you_like_added'];
		}
	}
	
	$forum_flash->add_info($message);
	redirect(forum_link($forum_url['post'], $post_id), $message);
}

