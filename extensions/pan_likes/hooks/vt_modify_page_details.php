<?php

if (!defined('FORUM')) die();


if (file_exists($ext_info['path'].'/lang/'.$forum_user['language'].'.php') )
	require $ext_info['path'].'/lang/'.$forum_user['language'].'.php';
else
	require $ext_info['path'].'/lang/English.php';


if (file_exists($ext_info['path'].'/style/'.$forum_user['style'].'/'.$ext_info['id'].'.css'))
	$forum_loader->add_css($ext_info['url'].'/style/'.$forum_user['style'].'/'.$ext_info['id'].'.css', array('type' => 'url', 'media' => 'screen'));
else
	$forum_loader->add_css($ext_info['url'].'/style/Oxygen/'.$ext_info['id'].'.css', array('type' => 'url', 'media' => 'screen'));


$pan_like_post_ips = $pan_like_user_ids = $pan_like_user_name = array();

$query = array(
	'SELECT'	=> 'pl.post_id, pl.user_id, pl.user_ip, u.username',
	'FROM'		=> 'pan_likes AS pl',
	'JOINS'		=> array(
		array(
			'INNER JOIN'	=> 'users AS u',
			'ON'			=> 'u.id=pl.user_id'
		),
	),
	'WHERE'		=> 'topic_id='.$id,
	'ORDER BY'	=> 'liked DESC',
);
($hook = get_hook('pan_likes_vt_modify_page_details_get_likes_info')) ? eval($hook) : null;
$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);

while ($rows = $forum_db->fetch_assoc($result))
{
	if (!isset($pan_like_user_ips[$rows['post_id']])) {
		$pan_like_user_ips[$rows['post_id']][] = $rows['user_ip'];
	} else {
		$pan_like_user_ips[$rows['post_id']][] = $rows['user_ip'];
	}
	
	if (!isset($pan_like_user_ids[$rows['post_id']])) {
		$pan_like_user_ids[$rows['post_id']][] = $rows['user_id'];
	} else {
		$pan_like_user_ids[$rows['post_id']][] = $rows['user_id'];
	}
	
	if (!isset($pan_like_user_name[$rows['post_id']])) {
		if ($rows['user_id'] == 1)
			$pan_like_user_name[$rows['post_id']][] = $rows['user_ip'];
		else
			$pan_like_user_name[$rows['post_id']][] = $rows['username'];
	} else {
		if ($rows['user_id'] == 1)
			$pan_like_user_name[$rows['post_id']][] = $rows['user_ip'];
		else
			$pan_like_user_name[$rows['post_id']][] = $rows['username'];
	}
}


