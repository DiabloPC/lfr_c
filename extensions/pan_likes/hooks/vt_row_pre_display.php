<?php

if (!defined('FORUM')) die();

$post_likes_sum = ($cur_post['post_likes'] > 0) ? $cur_post['post_likes'] : '';

//List Users
if (isset($pan_like_user_name[$cur_post['id']]))
	$liked_for_post = sprintf($lang_pan_likes['liked_for_post'], implode(', ', $pan_like_user_name[$cur_post['id']]));
else
	$liked_for_post = $lang_pan_likes['no_one_liked'];


if ($forum_user['is_guest'])
{
	if (isset($pan_like_user_ips[$cur_post['id']]) && in_array(get_remote_address(), $pan_like_user_ips[$cur_post['id']]))
		$post_likes_result = '<i class="like-img liked" title="'.$liked_for_post.'"></i><strong style="color:green;">'.$post_likes_sum.'</strong>';
	
	else if ($forum_user['g_pan_likes'] == '0')
		$post_likes_result = '<i class="like-img liked" title="'.$lang_pan_likes['pan_likes_guests'].'"></i><strong style="color:green;">'.$post_likes_sum.'</strong>';
	else
		$post_likes_result = '<a href="'.forum_link($forum_url['pan_likes_add'], array($id, $cur_post['id'], $cur_post['poster_id'])).'"><i class="like-img" title="'.$lang_pan_likes['i_like_it'].'"></i></a><strong style="color:green;">'.$post_likes_sum.'</strong>';
	
}
else
{
	if (isset($pan_like_user_ids[$cur_post['id']]) && in_array($forum_user['id'], $pan_like_user_ids[$cur_post['id']]))
		$post_likes_result = '<i class="like-img liked" title="'.$liked_for_post.'"></i><strong style="color:green;">'.$post_likes_sum.'</strong>';
	
	else if ($forum_user['id'] == $cur_post['poster_id'])
		$post_likes_result = '<i class="like-img liked" title="'.$liked_for_post.'"></i><strong style="color:green;">'.$post_likes_sum.'</strong>';
		
	else if ($forum_user['g_pan_likes'] == '0')
		$post_likes_result = '<i class="like-img liked" title="'.$lang_pan_likes['pan_likes_guests'].'"></i><strong style="color:green;">'.$post_likes_sum.'</strong>';
	else
		$post_likes_result = '<a href="'.forum_link($forum_url['pan_likes_add'], array($id, $cur_post['id'], $cur_post['poster_id'])).'"><i class="like-img" title="'.$lang_pan_likes['i_like_it'].'"></i></a><strong style="color:green;">'.$post_likes_sum.'</strong>';
}

$forum_page['post_options']['pan_likes'] = '<p class="post-likes">'.$post_likes_result.'</p>';




