<?php

if (!defined('FORUM')) die();

$group_legend = array(
	'position' 	=> 'Group position',
	'Show on legend' 	=> 'Show group on Group Legend',
	'Group legend' 	=> 'Group legend',
	'Int number' 	=> 'Groups on Group Legend are sorted depending on this parameter. The lower is number, the earlier group is showed on legend.',
);

?>
