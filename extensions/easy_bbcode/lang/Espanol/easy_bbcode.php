<?php
/*
*
*
*	Archivo de lenguaje español para hm easy bbcode por Himura de MyBB-Es - PunBB Hispano
*	
*	
*
*
*/
if (!defined('FORUM')) die();
$lang_ebbcode = array(
// admin
'acne' => 'Atención no existe el archivo cache/cache_bbcodes.php',
'acnelink' => 'click aquí para generarlo',
'edit' => 'Editar',
'del' => 'Borrar',
'nobb' => 'No hay BBcodes',
'rsus' => 'BBcodes Actualizados Correctamente',
'rerror' => 'Error al actualizar BBcodes',
'delc' => '¿Quieres Borrar este BBcode?',
'nbbc' => 'Crear Nuevo BBcode',
'name' => 'Nombre',
'exr' => 'Expresión Regular',
'rplz' => 'Reemplazo',
'atv' => 'Activo:',
'yes' => 'Sí',
'no' => 'No',
'devby' => 'Desarrollado Por',
'sup' => 'Soporte',
'codec' => 'Crear BBcode',
'ndesc' => 'Nombre que identifique el bbcode',
'exrd1' => 'Expresión regular a buscar en el mensaje. Ejemplo:',
'descav' => 'Avanzado:',
'rplzd1' => 'Cadena a reemplazar en el mensaje ejemplo:',
'atvdesc' => 'Este BBcode estará activo',
'cedit' => 'Editar BBcode',
// global header
'gcne' => 'Atención Administrador no existe el archivo cache/cache_bbcodes.php',
'gcnelink' => 'click aquí para generarlo'
);
?>