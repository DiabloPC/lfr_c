<?php
/*
*
*
*	English language file for hm easy bbcode by Himura of MyBB-Es - PunBB Hispano
*	
*
*
*/
if (!defined('FORUM')) die();
$lang_ebbcode = array(
// admin
'acne' => 'Warning the file cache/cache_bbcodes.php not there',
'acnelink' => 'Click here to generate',
'edit' => 'Edit',
'del' => 'Delete',
'nobb' => 'No BBcodes',
'rsus' => 'BBcodes Updated Correctly',
'rerror' => 'Failed to update BBcodes',
'delc' => 'Do you want to delete this BBcode?',
'nbbc' => 'Add New BBcode',
'name' => 'Name',
'exr' => 'Regular Expression',
'rplz' => 'Replacement',
'atv' => 'Active:',
'yes' => 'Yes',
'no' => 'No',
'devby' => 'Developed by',
'sup' => 'Support',
'codec' => 'Create BBcode',
'ndesc' => 'Name that identifies the bbcode',
'exrd1' => 'Regular expression to search the message. Example: ',
'descav' => 'Advanced:',
'rplzd1' => 'string to replace in the message. Example:',
'atvdesc' => 'This BBcode will be active',
'cedit' => 'Edit BBcode',
// global header
'gcne' => 'Warning Administrator, the file cache/cache_bbcodes.php not there',
'gcnelink' => 'Click here to generate'
);
?>