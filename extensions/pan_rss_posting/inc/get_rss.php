<?php if (!defined('FORUM')) die();

function PanRssPostingGetOptions($channel_info)
{
	global $forum_db;
	$output = true;

	if($channel_info['time_check'] == '1' && $channel_info['time_start'] > time())
		$output = false;

	if($channel_info['time_check'] == '1' && $channel_info['time_stop'] < time())
		$output = false;

	if($channel_info['time_interval'] != '0' && time() > $channel_info['time_next'])
	{
		$time_next = ($channel_info['time_interval']*60)+time();
		$query = array(
			'UPDATE'	=> 'pan_rss_posting',
			'SET'		=> 'time_next=\''.$time_next.'\'',
			'WHERE'		=> 'id='.$channel_info['id']
		);
		$forum_db->query_build($query) or error(__FILE__, __LINE__);

		PanRssPostingGenCache();

	} else 
	if ($channel_info['time_interval'] != '0' && time() < $channel_info['time_next'])
	{
		$output = false;
	}

	return $output;
}

function PanRssPostingFormatContent($channel_info, $item, $content)
{
	$text = html_entity_decode($content);
	@preg_match_all('#<img.*src="(.*)".*>#isU', $text, $match);

	if(isset($match[0]) && isset($match[1]))
	{
		$i = 0;
		foreach ($match[0] as $key => $val)
		{
			if(isset($match[0][$key]) && isset($match[1][$key]) && $channel_info['max_img'] > $i)
				$text = str_replace($match[0][$key], '[img]'.$match[1][$key].'[/img]'."\n", $text);

			$i++;
		}
	}

	//Transformation Tags
	$replace_tags = array(
		'<p>' => '',
		'</p>' => "\n",
		'<b>' => '[b]',
		'</b>' => '[/b]',
		'<strong>' => '[b]',
		'</strong>' => '[/b]',
	);
	foreach($replace_tags as $in => $out)
		$text = str_replace($in, $out, $text);

	$text = strip_tags($text);

//Crear Lincs From Content
//	if ($channel_info['clear_links'] == '1')
//		$text = preg_replace('/(http|https):\/\/.[\S]{0,}/iu', '', $text);

	$link = strip_tags($item->link);
	if($channel_info['source_link'] == '1' && !empty($link))
		$text .= "\n".'[url='.$link.']_источник[/url]';

	return $text;
}

if(file_exists(FORUM_ROOT.$forum_url['pan_rss_posting_cache'].'/pan_rss_posting_cache.php'))
	require FORUM_ROOT.$forum_url['pan_rss_posting_cache'].'/pan_rss_posting_cache.php';

if(isset($rss_posting_info))
{
	$topic_info = array();
	$query = array(
		'SELECT'	=> 'id, first_post_id, last_post, num_views, num_replies',
		'FROM'		=> 'topics',
//		'WHERE'		=> 'id='.implode(',', $tids)
	);
	$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
	while ($fetch_info = $forum_db->fetch_assoc($result)) {
		$topic_info[$fetch_info['id']] = $fetch_info;
	}


	$channel_output = array();
	//Разбиваем настройки кеша на ключ и массив настроек каждого канала
	foreach ($rss_posting_info as $key => $channel_info)
	{
		$validate = PanRssPostingGetOptions($channel_info);

		if ($validate === true)
		{
			//загружаем все новости канала
			$xml_files = @simplexml_load_file($channel_info['url'], null, LIBXML_NOCDATA);
			$item_output = array();

//Invalid argument supplied for foreach()
//if(isset($xml_files->channel) && is_array($xml_files->channel))
//{
			//Разбиваем канал на ленты и формируем массив
			foreach ($xml_files->channel->item as $item)
			{
				$pub_date = strtotime($item->pubDate);
				$title = strip_tags($item->title);
				$namespaces = $item->getNameSpaces(true);
				
				//content:encoded
				if(isset($namespaces['content'])) {
					$content_enc = $item->children($namespaces['content']);
					$content = PanRssPostingFormatContent($channel_info, $item, $content_enc->encoded);
				} else if (isset($item->description)) {
					$content = PanRssPostingFormatContent($channel_info, $item, $item->description);
				} else {
					$content = '';
				}
				//dc:creator
				if(isset($namespaces['dc'])) {
					$dc = $item->children($namespaces['dc']);
					$author_name = strip_tags($dc->creator);
				} else { $author_name = ''; }
				$link = strip_tags($item->link);

				$item_info = array(
					'pub_date'		=> $pub_date,
					'title'			=> $title,
					'content'		=> $content,
					'author_name'	=> $author_name,
					'link'			=> $link,
				);
				//Добавляем в массив только не добавленные новости && чтобы дата не превышала текущую
				if ($pub_date > $topic_info[$channel_info['topic_id']]['last_post'] && $pub_date < time()) {
					$item_output[$pub_date] = $item_info;
					ksort($item_output);
				}
			}
			$channel_output[$channel_info['id']] = $item_output;
//}
		}
	}
//echo '<pre>';
//print_r($channel_output);
//echo '</pre>';
	
	//Разбить массив кеша
	foreach ($rss_posting_info as $ids => $posting_info)
	{
		
		if(isset($channel_output[$ids]) && is_array($channel_output[$ids]))
		{
			$i_post = 0;
			$i_topic = 0;
			//разбить массив настроек каждого канала
			foreach ($channel_output[$ids] as $pub_date => $item_info)
			{
				if($posting_info['topic_id'] > 0 && ($item_info['pub_date'] > $topic_info[$posting_info['topic_id']]['last_post']) && $posting_info['num_posts'] > $i_post)
				{
					$post_info = array(
						'is_guest'		=> $posting_info['user_id'] == '1' && !empty($item_info['author_name']) ? $item_info['author_name'] : '',
						'poster'		=> $posting_info['user_id'] == '1' && !empty($item_info['author_name']) ? $item_info['author_name'] : $posting_info['username'],
						'poster_id'		=> $posting_info['user_id'],
						'poster_email'	=> null,
						'subject'		=> isset($item_info['title']) ? $item_info['title'] : null,
						'message'		=> isset($item_info['content']) ? $item_info['content'] : '',
						'hide_smilies'	=> 0,
						'posted'		=> isset($item_info['pub_date']) ? $item_info['pub_date'] : time(),
						'subscr_action'	=> 0,
						'topic_id'		=> $posting_info['topic_id'],
						'forum_id'		=> $posting_info['forum_id'],
						'update_user'	=> true,
						'update_unread'	=> true
					);

					($hook = get_hook('pan_rss_posting_pre_add_post')) ? eval($hook) : null;
					add_post($post_info, $new_pid);

					if ( $posting_info['title_msg'] == '1' && ($new_pid > 0) && $forum_db->field_exists('posts', 'post_title'))
					{
						$query = array(
							'UPDATE'	=> 'posts',
							'SET'		=> 'post_title=\''.$forum_db->escape($post_info['subject']).'\'',
							'WHERE'		=> 'id='.$new_pid
						);
						$forum_db->query_build($query) or error(__FILE__, __LINE__);
					}

					$num_replies = isset($topic_info[$posting_info['topic_id']]['num_replies']) ? $topic_info[$posting_info['topic_id']]['num_replies'] : '0';
					$first_post_id = isset($topic_info[$posting_info['topic_id']]['first_post_id']) ? $topic_info[$posting_info['topic_id']]['first_post_id'] : '0';
					if ($posting_info['cleaning'] == '2' && $posting_info['max_msg'] < $num_replies && $first_post_id > 0)
					{
						$query = array(
							'SELECT'	=> 'id',
							'FROM'		=> 'posts',
							'WHERE'		=> 'topic_id='.$posting_info['topic_id'].' AND id!='.$first_post_id,
							'ORDER BY'	=> 'id ASC',
							'LIMIT'		=> '1'
						);
						$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
						$del_post = $forum_db->fetch_assoc($result);

						if (isset($del_post['id'])) {
							//delete_post($id, $tid, $fid);
							delete_post($del_post['id'], $posting_info['topic_id'], $posting_info['forum_id']);
						}
					}
					
					$i_post++;
					
				} else if ($posting_info['topic_id'] == '0' && !empty($item_info['title']) && $posting_info['num_topics'] > $i_topic) {
					
					$query = array(
						'SELECT'	=> 'id',
						'FROM'		=> 'topics',
						'WHERE'		=> 'subject=\''.$forum_db->escape($item_info['title']).'\'',
					);
					$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);

					if(!$forum_db->num_rows($result) > 0)
					{
						$post_info = array(
							'is_guest'		=> '',
							'poster'		=> $posting_info['user_id'] == '1' && !empty($item_info['author_name']) ? $item_info['author_name'] : $posting_info['username'],
							'poster_id'		=> $posting_info['user_id'],
							'poster_email'	=> null,
							'subject'		=> isset($item_info['title']) ? $item_info['title'] : '',
							'message'		=> isset($item_info['content']) ? $item_info['content'] : '',
							'hide_smilies'	=> 0,
							'posted'		=> isset($item_info['pub_date']) ? $item_info['pub_date'] : time(),
							'subscribe'		=> 0,
							'forum_id'		=> $posting_info['forum_id'],
							'forum_name'	=> $posting_info['forum_name'],
							'update_user'	=> true,
							'update_unread'	=> true
						);

						($hook = get_hook('pan_rss_posting_pre_add_topic')) ? eval($hook) : null;
						add_topic($post_info, $new_tid, $new_pid);

						$i_topic++;

					}
				}
			}
		}

		if ($posting_info['cleaning'] == '1')
		{
			$num_views = isset($topic_info[$posting_info['topic_id']]['num_views']) ? $topic_info[$posting_info['topic_id']]['num_views'] : '0';
			$num_replies = isset($topic_info[$posting_info['topic_id']]['num_replies']) ? $topic_info[$posting_info['topic_id']]['num_replies'] : '0';

			if ($posting_info['max_views'] == $num_views || $posting_info['max_msg'] == $num_replies || $posting_info['time_stop'] < time())
			{
				$query = array(
					'DELETE'	=> 'pan_rss_posting',
					'WHERE'		=> 'id='.$posting_info['id']
				);
				$forum_db->query_build($query) or error(__FILE__, __LINE__);
			}
		}
	}
}


