<?php

function PanRssPostingGenCache()
{
	global $forum_db, $forum_url;

	$output = array();
	$query = array(
		'SELECT'	=> '*',
		'FROM'		=> 'pan_rss_posting',
	);
	$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
	while ($rss_info = $forum_db->fetch_assoc($result))
	{
		$output[$rss_info['id']] = $rss_info;
	}

	$fh = @fopen(FORUM_ROOT.$forum_url['pan_rss_posting_cache'].'/pan_rss_posting_cache.php', 'wb');
	if (!$fh)
		error('Unable to write censor cache file to cache directory. Please make sure PHP has write access to the directory \'cache\'.', __FILE__, __LINE__);

	fwrite($fh, '<?php'."\n\n".'$rss_posting_info = '.var_export($output, true).';'."\n\n".'?>');
	fclose($fh);
}

function PanRssPostingGetTopicInfo()
{
	global $rss_posting_info, $id;

	$output = array();
	foreach ($rss_posting_info as $key => $posting_info)
	{
		if($posting_info['topic_id'] == $id) {
			foreach ($posting_info as $keys => $topic_info)
			{
				$output[$keys] = $topic_info;
			}
		}
	}

	return $output;
}

