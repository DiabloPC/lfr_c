<?php if (!defined('FORUM')) die();

if ($forum_user['g_id'] == FORUM_ADMIN && FORUM_PAGE_SECTION == 'management')
{
	$forum_page['admin_submenu']['pan-rss-posting'] = '<li class="'.((FORUM_PAGE == 'admin-pan-rss-posting') ? 'active' : 'normal').((empty($forum_page['admin_submenu'])) ? ' first-item' : '').'"><a href="'.forum_link($forum_url['pan_rss_posting_config']).'">RSS Posting</span></a></li>';
}