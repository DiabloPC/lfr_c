<?php

$lang_pan_rss_posting = array (
	'rss_posting'			=> 'RSS постинг',
	'new_added'				=> 'RSS канал добавлен',
	'updated'				=> 'RSS канал обновлен',
	'deleted'				=> 'RSS канал удален',
	'add_new'				=> 'Добавить новый',
	'update'				=> 'Обновить',
	'delete'				=> 'Удалить',
	
	
	
	'posting_to_topic'		=> 'Постинг в тему: ',
	'posting_to_forum'		=> 'Постинг в форум: ',
	'no_channel'			=> 'Не создано ни одного канала.',
);

