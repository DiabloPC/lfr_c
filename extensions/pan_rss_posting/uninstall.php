<?php if(!defined('PAN_UNINSTALL')) die();

if ($forum_db->table_exists('pan_rss_posting'))
	$forum_db->drop_table('pan_rss_posting');

if(file_exists(FORUM_ROOT.$forum_url['pan_rss_posting_cache'].'/pan_rss_posting_cache.php'))
	unlink(FORUM_ROOT.$forum_url['pan_rss_posting_cache'].'/pan_rss_posting_cache.php');


