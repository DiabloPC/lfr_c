<?php if (!defined('FORUM_ROOT')) define('FORUM_ROOT', '../../../');

require FORUM_ROOT.'include/common.php';
require FORUM_ROOT.'include/common_admin.php';

if ($forum_user['g_id'] != FORUM_ADMIN)
	message($lang_common['No permission']);

// Load the admin.php language files
require FORUM_ROOT.'lang/'.$forum_user['language'].'/admin_common.php';
require FORUM_ROOT.'lang/'.$forum_user['language'].'/admin_index.php';
require FORUM_ROOT.'lang/'.$forum_user['language'].'/admin_users.php';

if(file_exists(FORUM_ROOT.'extensions/pan_rss_posting/lang/'.$forum_user['language'].'/pan_rss_posting.php'))
	require FORUM_ROOT.'extensions/pan_rss_posting/lang/'.$forum_user['language'].'/pan_rss_posting.php';
else
	require FORUM_ROOT.'extensions/pan_rss_posting/lang/English/pan_rss_posting.php';

if (isset($_POST['add_new']))
{
	$rss_url = forum_trim($_POST['rss_url']);
	$rss_cms = forum_trim($_POST['rss_cms']);
	$rss_fid = isset($_POST['rss_fid']) ? intval($_POST['rss_fid']) : '0';
	$rss_tid = isset($_POST['rss_tid']) ? intval($_POST['rss_tid']) : '0';
	$rss_user_id = isset($_POST['rss_user_id']) ? intval($_POST['rss_user_id']) : '1';
	$rss_time_interval = !empty($_POST['rss_time_interval']) ? $_POST['rss_time_interval'] : '0' ;
	$time_next = time()-$rss_time_interval;
	$time_check = $_POST['time_check'];
	$time_start = !empty($_POST['time_start']) ? strtotime($_POST['time_start']) : time();
	$time_stop = !empty($_POST['time_stop']) ? strtotime($_POST['time_stop']) : time();
	$num_topics = intval($_POST['num_topics']);
	$num_posts = intval($_POST['num_posts']);
	$max_msg = intval($_POST['max_msg']);
	$max_views = intval($_POST['max_views']);
	$max_img = intval($_POST['max_img']);
	$title_msg = $_POST['title_msg'];
	$clear_links = $_POST['clear_links'];
	$source_link = $_POST['source_link'];
	$reverse_msg = $_POST['reverse_msg'];
	$cleaning = $_POST['cleaning'];
	
	$query = array(
		'SELECT'	=> 'forum_name',
		'FROM'		=> 'forums',
		'WHERE'		=> 'id='.$rss_fid,
	);
	$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
	$forums_info = $forum_db->fetch_assoc($result);
	
	if($rss_user_id != '0')
	{
		$query = array(
			'SELECT'	=> 'id, username',
			'FROM'		=> 'users',
			'WHERE'		=> 'id='.$rss_user_id,
		);
		$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
		$user_info = $forum_db->fetch_assoc($result);
		$rss_username = $user_info['username'];
	} else {
		$rss_username = 'Guest';
	}

	$query = array(
		'INSERT'	=> 'url, cms, forum_id, forum_name, topic_id, username, user_id, time_interval, time_next, time_check, time_start, time_stop, num_topics, num_posts, max_msg, max_views, max_img, title_msg, clear_links, source_link, reverse_msg, cleaning',
		'INTO'		=> 'pan_rss_posting',
		'VALUES'	=> '\''.$forum_db->escape($rss_url).'\', \''.$forum_db->escape($rss_cms).'\', \''.$rss_fid.'\', \''.$forum_db->escape($forums_info['forum_name']).'\', \''.$rss_tid.'\', \''.$forum_db->escape($rss_username).'\', \''.$rss_user_id.'\', \''.$rss_time_interval.'\', \''.$time_next.'\', \''.$time_check.'\', \''.$time_start.'\', \''.$time_stop.'\', \''.$num_topics.'\', \''.$num_posts.'\', \''.$max_msg.'\', \''.$max_views.'\', \''.$max_img.'\', \''.$title_msg.'\', \''.$clear_links.'\', \''.$source_link.'\', \''.$reverse_msg.'\', \''.$cleaning.'\''
	);
	$forum_db->query_build($query) or error(__FILE__, __LINE__);

	PanRssPostingGenCache();

	$forum_flash->add_info($lang_pan_rss_posting['new_added']);
	redirect(forum_link($forum_url['pan_rss_posting_config']), $lang_pan_rss_posting['new_added']);
}

if (isset($_POST['update']))
{
	$rss_id = $_POST['rss_id'];
	$rss_url = forum_trim($_POST['rss_url']);
	$rss_user_id = $_POST['rss_user_id'];
	$rss_time_interval = !empty($_POST['rss_time_interval']) ? $_POST['rss_time_interval'] : '0' ;
	$time_next = time()-$rss_time_interval;
	$time_check = $_POST['time_check'];
	$time_start = !empty($_POST['time_start']) ? strtotime($_POST['time_start']) : time();
	$time_stop = !empty($_POST['time_stop']) ? strtotime($_POST['time_stop']) : time();
	$num_topics = $_POST['num_topics'];
	$num_posts = $_POST['num_posts'];
	$max_msg = $_POST['max_msg'];
	$max_views = $_POST['max_views'];
	$max_img = $_POST['max_img'];
	$title_msg = $_POST['title_msg'];
	$clear_links = $_POST['clear_links'];
	$source_link = $_POST['source_link'];
	$reverse_msg = $_POST['reverse_msg'];
	$cleaning = $_POST['cleaning'];

	if($rss_user_id != '0')
	{
		$query = array(
			'SELECT'	=> 'id, username',
			'FROM'		=> 'users',
			'WHERE'		=> 'id='.$rss_user_id,
		);
		$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
		$user_info = $forum_db->fetch_assoc($result);
		$rss_username = $user_info['username'];
	} else {
		$rss_username = 'Guest';
	}

	$query = array(
		'UPDATE'	=> 'pan_rss_posting',
		'SET'		=> 'url=\''.$rss_url.'\', user_id=\''.$rss_user_id.'\', time_interval=\''.$rss_time_interval.'\', time_next=\''.$time_next.'\', time_check=\''.$time_check.'\', time_start=\''.$time_start.'\', time_stop=\''.$time_stop.'\', num_topics=\''.$num_topics.'\', num_posts=\''.$num_posts.'\', max_msg=\''.$max_msg.'\', max_views=\''.$max_views.'\', max_img=\''.$max_img.'\', title_msg=\''.$title_msg.'\', clear_links=\''.$clear_links.'\', source_link=\''.$source_link.'\', reverse_msg=\''.$reverse_msg.'\', cleaning=\''.$cleaning.'\'',
		'WHERE'		=> 'id='.$rss_id
	);
	$forum_db->query_build($query) or error(__FILE__, __LINE__);

	PanRssPostingGenCache();

	$forum_flash->add_info($lang_pan_rss_posting['updated']);
	redirect(forum_link($forum_url['pan_rss_posting_config']), $lang_pan_rss_posting['updated']);
}

if (isset($_POST['delete']))
{
	$rss_id = $_POST['rss_id'];

	$query = array(
		'DELETE'	=> 'pan_rss_posting',
		'WHERE'		=> 'id='.$rss_id
	);
	$forum_db->query_build($query) or error(__FILE__, __LINE__);

	PanRssPostingGenCache();

	$forum_flash->add_info($lang_pan_rss_posting['deleted']);
	redirect(forum_link($forum_url['pan_rss_posting_config']), $lang_pan_rss_posting['deleted']);
}

$forum_page['group_count'] = $forum_page['item_count'] = $forum_page['fld_count'] = 0;

$forum_page['crumbs'] = array(
	array($forum_config['o_board_title'], forum_link($forum_url['index'])),
	array($lang_admin_common['Forum administration'], forum_link($forum_url['admin_index'])),
	array($lang_admin_common['Management'], forum_link($forum_url['admin_reports'])),
	$lang_pan_rss_posting['rss_posting']
);

define('FORUM_PAGE_SECTION', 'management');
define('FORUM_PAGE', 'admin-pan-rss-posting');
require FORUM_ROOT.'header.php';

// START SUBST - <!-- forum_main -->
ob_start();

?>
<script>
function getForumId(fid)
{
	jQuery.ajax({
		url:	"<?php echo $base_url.'/extensions/pan_rss_posting/ajax/topics.php' ?>",
		type:	"POST",
		dataType: "html",
		data: jQuery("#rss_forum_id").serialize(),
		success: function(response){
			document.getElementById("result_div_id").innerHTML = response;},
		error: function(response){
			document.getElementById("result_div_id").innerHTML = "Ошибка при отправке формы";}
	});
}

function topicOrForum(tid)
{
	if (tid != "0"){
		document.getElementById("num_topics").style.display = 'none';
		document.getElementById("num_posts").style.display = 'block';
	}else{
		document.getElementById("num_topics").style.display = 'block';
		document.getElementById("num_posts").style.display = 'none';
	}
}

function hideChannel(id){
	if(document.getElementById(id).style.display != "none"){
		document.getElementById(id).style.display = 'none';
	}else{
		document.getElementById(id).style.display = '';}
}
</script>

<div class="main-subhead">
	<h2 class="hn"><span><a onclick="hideChannel('channel_0'); return false;" href="#">Создать новый постинг RSS ленты</a></span></h2>
</div>

<div class="main-content main-frm">

	<div id="channel_0" style="display:none">

		<form class="frm-form" method="post" accept-charset="utf-8" action="<?php echo forum_link($forum_url['pan_rss_posting_config']) ?>">
			<div class="hidden">
				<input type="hidden" name="csrf_token" value="<?php echo generate_form_token(forum_link($forum_url['pan_rss_posting_config'])) ?>" />
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>URL канала</span><small>Введите ссылку на канал новостей</small></label><br />
					<span class="fld-input"><input type="url" id="fld" name="rss_url" size="50" maxlength="255" value="" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box select">
					<label for="fld"><span>CMS</span><small>Выбрать систему управления сайтом</small></label><br />
					<span class="fld-input"><select id="fld" name="rss_cms">
<?php
$arr_cms = array(
	'punbb'		=> 'PunBB',
	'wordpress' => 'WordPress',
);
foreach ($arr_cms as $key => $cms){
	echo "\t\t\t\t\t\t\t\t".'<option value="'.$key.'">'.$cms.'</option>'."\n";
}
?>
					</select></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box select">
					<label for="fld"><span>Форумы</span><small>Выбрать форум для постинга</small></label><br />
					<span class="fld-input"><select id="rss_forum_id" name="rss_fid" onchange="getForumId(this.value);">
						<option value="0" selected="selected">Выбрать форум из списка</option>
<?php
$forum_name = array();
$query = array(
	'SELECT'	=> 'id, forum_name',
	'FROM'		=> 'forums',
);
$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
while ($forums_info = $forum_db->fetch_assoc($result))
	$forum_name[$forums_info['id']] = $forums_info;

foreach ($forum_name as $forums_info)
	echo "\t\t\t\t\t\t\t\t".'<option value="'.$forums_info['id'].'">'.$forums_info['forum_name'].'</option>'."\n";
?>
					</select></span>
				</div>
			</div>

			<div id="result_div_id"></div>

<?php
$topic_name = array();
$query = array(
	'SELECT'	=> 'id, subject',
	'FROM'		=> 'topics',
);
$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
while ($topic_info = $forum_db->fetch_assoc($result))
	$topic_name[$topic_info['id']] = $topic_info;
?>

			<div class="sf-set set">
				<div class="sf-box select">
					<label for="fld"><span>Пользователи</span><small>Выбрать пользователя как постера. По умолчанию постером будет являться истинный автор</small></label><br />
					<span class="fld-input"><select id="fld" name="rss_user_id">
<?php
$users_info = array();
$query = array(
	'SELECT'	=> 'id, username',
	'FROM'		=> 'users',
);
$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
while ($fetch_users = $forum_db->fetch_assoc($result))
	$users_info[$fetch_users['id']] = $fetch_users;

foreach ($users_info as $user_info)
	echo "\t\t\t\t\t\t\t\t".'<option value="'.$user_info['id'].'">'.$user_info['username'].'</option>'."\n";
?>
					</select></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>Интервал</span><small>Интервал между проверкой новостей в минутах. Установите - 0 для отключения. 24 часа = 1440 минут.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="rss_time_interval" size="35" maxlength="5" value="1440" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="time_check" value="0" />
					<span class="fld-input" onclick="hideChannel('time_check_0');"><input type="checkbox" name="time_check" value="1" /></span>
					<label for="fld "><span>Временный постинг</span>Включить временный постинг. Постинг будет осуществлятся в рамках установленного ниже времени</label>
				</div>
			</div>

			<fieldset class="mf-set set" id="time_check_0" style="display:none">
				<legend><span>Дата</span></legend>
				<div class="mf-box">

					<div class="mf-field text">
						<label for="fld<?php echo ++$forum_page['fld_count'] ?>"><span class="fld-label">Начало</span></label><br />
						<span class="fld-input"><input type="date" id="fld" name="time_start" value="<?php echo date("Y-m-d", time()) ?>" size="20" /></span>
					</div>

					<div class="mf-field text">
						<label for="fld<?php echo ++$forum_page['fld_count'] ?>"><span class="fld-label">Завершение</span></label><br />
						<span class="fld-input"><input type="date" id="fld" name="time_stop" value="<?php echo date("Y-m-d", time()) ?>" size="20" /></span>
					</div>

				</div>
			</fieldset>

			<div class="sf-set set" id="num_topics" style="display:block">
				<div class="sf-box text">
					<label for="fld"><span>Тем за 1 раз</span><small>Количество тем добавляемых за одну проверку</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="num_topics" size="35" maxlength="2" value="1" /></span>
				</div>
			</div>

			<div class="sf-set set" id="num_posts" style="display:none">
				<div class="sf-box text">
					<label for="fld"><span>Сообщений за 1 раз</span><small>Количество сообщениий добавляемых за одну проверку</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="num_posts" size="35" maxlength="2" value="1" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>MAX сообщений</span><small>Максимальное количество сообщениий в теме  при достижении которых канал постинга становится неактивным. Если установлен 0 - отключено.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="max_msg" size="35" maxlength="3" value="0" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>MAX просмотров</span><small>Максимальное количество просмотров темы  при достижении которых канал постинга становится неактивным. Если установлен 0 - отключено.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="max_views" size="35" maxlength="6" value="0" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>Количество изображений</span><small>Максимальное количество изображений в одном сообщении. Если установлен 0 - отключено.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="max_img" size="35" maxlength="2" value="0" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="title_msg" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="title_msg" value="1" /></span>
						<label for="fld "><span>Заголовок сообщения</span>генерировать заголовок для каждого сообщения. (Эта опция работает только при установленном расширении - Header Each In Message)</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="clear_links" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="clear_links" value="1" /></span>
						<label for="fld "><span>Удаление ссылок</span>Очищать сообщения от ссылок.</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="source_link" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="source_link" value="1" /></span>
						<label for="fld "><span>Ссылка на источник</span>Добавлять ссылку на источник материала в конце поста.</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="reverse_msg" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="reverse_msg" value="1" /></span>
						<label for="fld "><span>Реверс сообщений</span>Включить отображение сообщений в обратном порядке.</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box select">
					<label for="fld"><span>Цели</span><small>Действия при достижения цели</small></label><br />
					<span class="fld-input"><select id="fld" name="cleaning">
<?php
$cleaning = array(
	'0'	=> 'Отключено',
	'1' => 'Удалять канал',
	'2' => 'Удалять старые сообщения',
);

foreach ($cleaning as $key => $val){
	echo "\t\t\t\t\t\t\t\t".'<option value="'.$key.'">'.$val.'</option>'."\n";
}
?>
					</select></span>
				</div>
			</div>

			<div class="frm-buttons">
				<span class="submit primary"><input type="submit" name="add_new" value="Добавить канал" /></span>
			</div>
		</form>
	
	</div>
</div>






<div class="main-content main-frm">
	<div class="main-head">
		<h2 class="hn"><span>Список каналов RSS постинга</span></h2>
	</div>
<?php
	$query = array(
		'SELECT'	=> '*',
		'FROM'		=> 'pan_rss_posting',
		'ORDER BY'	=> 'forum_id',
	);
	$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);
	if ($forum_db->num_rows($result) > 0)
	{
		$forum_page['group_count'] = $forum_page['item_count'] = 0;
		while ($rss_info = $forum_db->fetch_assoc($result))
		{
?>

	<form class="frm-form" method="post" accept-charset="utf-8" action="<?php echo forum_link($forum_url['pan_rss_posting_config']) ?>">
		<div class="hidden">
			<input type="hidden" name="csrf_token" value="<?php echo generate_form_token(forum_link($forum_url['pan_rss_posting_config'])) ?>" />
			<input type="hidden" name="rss_id" value="<?php echo $rss_info['id'] ?>" />
		</div>

		<div class="content-head">
			<h2 class="hn">
				<span><?php echo $rss_info['topic_id'] != '0' ? $lang_pan_rss_posting['posting_to_topic'] : $lang_pan_rss_posting['posting_to_forum'] ?><strong><?php echo $rss_info['topic_id'] != '0' ? '<a href="'.forum_link($forum_url['topic'], array($rss_info['topic_id'], sef_friendly($topic_name[$rss_info['topic_id']]['subject']))).'" target="_blank">'.$topic_name[$rss_info['topic_id']]['subject'].'</a>' : '<a href="'.forum_link($forum_url['forum'], array($rss_info['forum_id'], sef_friendly($forum_name[$rss_info['forum_id']]['forum_name']))).'" target="_blank">'.$forum_name[$rss_info['forum_id']]['forum_name'].'</a>' ?></strong>
				<a onclick="hideChannel('channel_<?php echo $rss_info['id'] ?>'); return false;" href="#"> (подробные настройки)</a>
				</span>
			</h2>
		</div>

		<div class="sf-set set">
			<div class="sf-box text">
				<label for="fld"><span>URL канала</span><small>Введите ссылку на канал новостей</small></label><br />
				<span class="fld-input"><input type="url" id="fld" name="rss_url" size="50" maxlength="255" value="<?php echo $rss_info['url'] ?>" /></span>
			</div>
		</div>

		<div id="channel_<?php echo $rss_info['id'] ?>" style="display:none">

			<div class="sf-set set">
				<div class="sf-box select">
					<label for="fld"><span>Пользователи</span><small>Выбрать пользователя как постера. По умолчанию постером будет являться истинный автор</small></label><br />
					<span class="fld-input"><select id="fld" name="rss_user_id">
<?php
foreach ($users_info as $user_info)
	if($rss_info['user_id'] == $user_info['id'])
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$user_info['id'].'" selected="selected">'.$user_info['username'].'</option>'."\n";
	else
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$user_info['id'].'">'.$user_info['username'].'</option>'."\n";
?>
					</select></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>Интервал</span><small>Интервал между проверкой новостей в минутах. Установите - 0 для отключения. 24 часа = 1440 минут.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="rss_time_interval" size="35" maxlength="5" value="<?php echo $rss_info['time_interval'] ?>" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="time_check" value="0" />
						<span class="fld-input"  onclick="hideChannel('time_check_<?php echo $rss_info['id'] ?>');"><input type="checkbox" id="fld" name="time_check" <?php if($rss_info['time_check'] == '1') echo 'checked="checked"' ?> value="1" /></span>
						<label for="fld "><span>Временный постинг</span>Включить временный постинг. Постинг будет осуществлятся в рамках установленного ниже времени</label>
				</div>
			</div>

			<fieldset class="mf-set set" id="time_check_<?php echo $rss_info['id'] ?>" style="display:<?php echo ($rss_info['time_check'] == '1') ? 'blok' : 'none' ?>">
				<legend><span>Дата</span></legend>
				<div class="mf-box">

					<div class="mf-field text">
						<label for="fld<?php echo ++$forum_page['fld_count'] ?>"><span class="fld-label">Начало</span></label><br />
						<span class="fld-input"><input type="date" id="fld" name="time_start" value="<?php echo date("Y-m-d", $rss_info['time_start']) ?>" size="20" /></span>
					</div>

					<div class="mf-field text">
						<label for="fld<?php echo ++$forum_page['fld_count'] ?>"><span class="fld-label">Завершение</span></label><br />
						<span class="fld-input"><input type="date" id="fld" name="time_stop" value="<?php echo date("Y-m-d", $rss_info['time_stop']) ?>" size="20" /></span>
					</div>

				</div>
			</fieldset>

			<div class="sf-set set" style="display:<?php echo ($rss_info['topic_id'] == '0') ? 'blok' : 'none' ?>">
				<div class="sf-box text">
					<label for="fld"><span>Тем за 1 раз</span><small>Количество тем добавляемых за одну проверку</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="num_topics" size="35" maxlength="2" value="<?php echo $rss_info['num_topics'] ?>" /></span>
				</div>
			</div>

			<div class="sf-set set" style="display:<?php echo ($rss_info['topic_id'] != '0') ? 'blok' : 'none' ?>">
				<div class="sf-box text">
					<label for="fld"><span>Сообщений за 1 раз</span><small>Количество сообщениий добавляемых за одну проверку</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="num_posts" size="35" maxlength="2" value="<?php echo $rss_info['num_posts'] ?>" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>MAX сообщений</span><small>Максимальное количество сообщениий в теме при достижении которых канал постинга становится неактивным. Если установлен 0 - отключено.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="max_msg" size="35" maxlength="3" value="<?php echo $rss_info['max_msg'] ?>" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>MAX просмотров</span><small>Максимальное количество просмотров темы при достижении которых канал постинга становится неактивным. Если установлен 0 - отключено.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="max_views" size="35" maxlength="6" value="<?php echo $rss_info['max_views'] ?>" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box text">
					<label for="fld"><span>Количество изображений</span><small>Максимальное количество изображений в одном сообщении. Если установлен 0 - отключено.</small></label><br />
					<span class="fld-input"><input type="number" id="fld" name="max_img" size="35" maxlength="2" value="<?php echo $rss_info['max_img'] ?>" /></span>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="title_msg" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="title_msg" value="1" <?php if($rss_info['title_msg'] == '1') echo 'checked="checked"' ?> /></span>
						<label for="fld "><span>Заголовок сообщения</span>генерировать заголовок для каждого сообщения. (Эта опция работает только при установленном расширении - Header Each In Message)</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="clear_links" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="clear_links" value="1" <?php if($rss_info['clear_links'] == '1') echo 'checked="checked"' ?> /></span>
						<label for="fld "><span>Удаление ссылок</span>Очистить сообщение от ссылок.</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="source_link" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="source_link" value="1" <?php if($rss_info['source_link'] == '1') echo 'checked="checked"' ?> /></span>
						<label for="fld "><span>Ссылка на источник</span>Добавлять ссылку на источник материала в конце поста.</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box checkbox">
					<input type="hidden" name="reverse_msg" value="0" />
						<span class="fld-input"><input type="checkbox" id="fld" name="reverse_msg" value="1"<?php if($rss_info['reverse_msg'] == '1') echo 'checked="checked"' ?> /></span>
						<label for="fld "><span>Реверс сообщений</span>Включить отображение сообщений в обратном порядке.</label>
				</div>
			</div>

			<div class="sf-set set">
				<div class="sf-box select">
					<label for="fld"><span>Цели</span><small>Действия при достижения цели</small></label><br />
					<span class="fld-input"><select id="fld" name="cleaning">
<?php
$cleaning = array(
	'0'	=> 'Отключено',
	'1' => 'Удалять канал',
	'2' => 'Удалять старые сообщения',
);

foreach ($cleaning as $key => $val) {
	if($rss_info['cleaning'] == $key)
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$key.'" selected="selected">'.$val.'</option>'."\n";
	else
		echo "\t\t\t\t\t\t\t\t".'<option value="'.$key.'">'.$val.'</option>'."\n";
}
?>
					</select></span>
				</div>
			</div>

		</div>

		<div class="frm-buttons">
			<span class="submit primary"><input type="submit" name="update" value="<?php echo $lang_pan_rss_posting['update'] ?>" /></span>
			<span class="submit primary"><input type="submit" name="delete" value="<?php echo $lang_pan_rss_posting['delete'] ?>" /></span>
		</div>

	</form>

<?php
		}

	} else {
?>
	<div class="frm-form">
		<div class="ct-box">
			<p><?php echo $lang_pan_rss_posting['no_channel'] ?></p>
		</div>
	</div>
<?php
	}
?>
</div>
<?php

$tpl_temp = forum_trim(ob_get_contents());
$tpl_main = str_replace('<!-- forum_main -->', $tpl_temp, $tpl_main);
ob_end_clean();
// END SUBST - <!-- forum_main -->

require FORUM_ROOT.'footer.php';