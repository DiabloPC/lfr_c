<?php if (!defined('FORUM_ROOT')) define('FORUM_ROOT', '../../../');
define('FORUM_SKIP_CSRF_CONFIRM', 1);
require FORUM_ROOT.'include/common.php';
header('Content-type: text/html; charset=utf-8');

if ($forum_user['g_id'] != FORUM_ADMIN)
	message($lang_common['No permission']);

if(isset($_POST['rss_fid']))
{
	$fid = intval($_POST['rss_fid']);

	$topics = array();
	$query = array(
		'SELECT'	=> 'id, subject',
		'FROM'		=> 'topics',
		'WHERE'		=> 'forum_id='.$fid
	);
	$result = $forum_db->query_build($query) or error(__FILE__, __LINE__);

	echo '<div class="sf-set set"><div class="sf-box select"><label for="fld"><span>Темы</span><small>Выбрать тему для постинга в нее сообщений. Если тема для публикации сообщений не выбрана, то будет создана новая тема</small></label><br /><span class="fld-input"><select id="fld" name="rss_tid" onchange="topicOrForum(this.value);"><option value="0" selected="selected">Тема не выбрана</option>';

	while ($topic_info = $forum_db->fetch_assoc($result)) 
		echo '<option value="'.$topic_info['id'].'">'.forum_htmlencode($topic_info['subject']).'</option>'."\n";

	echo '</select></span></div></div>';
}

//require FORUM_ROOT.'footer.php';