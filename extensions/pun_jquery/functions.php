<?php
if (!defined('FORUM_ROOT'))
	define('FORUM_ROOT', '../../');
require FORUM_ROOT.'include/common.php';
require FORUM_ROOT.'include/common_admin.php';

define('FORUM_SKIP_CSRF_CONFIRM', 1);
define('FORUM_DISABLE_CSRF_CONFIRM', 1);

if($forum_user['g_id'] == FORUM_ADMIN)
{
	if ($forum_config['o_pun_jquery_version'] == 0) { $cur_jv = 2; } else { $cur_jv = 1; }
	$jquery_latest_content = @file_get_contents('http://cdn.jsdelivr.net/jquery/'.$cur_jv.'/jquery.min.js');
    preg_match('/v(\d+\.\d+\.\d+)/', $jquery_latest_content, $matches);
    $jquery_latest_version = $matches[1];

    $jquery_dir = FORUM_ROOT.'extensions/pun_jquery/js/';
	@chmod($jquery_dir, 0777);
	$result = @file_put_contents($jquery_dir.'jquery-'.$jquery_latest_version.'.min.js', $jquery_latest_content);
    if ($result)
    {
		if (file_exists($jquery_dir.'jquery-'.$forum_config['o_pun_jquery_'.$cur_jv.'x_version_number'].'.min.js'))
			unlink($jquery_dir.'jquery-'.$forum_config['o_pun_jquery_'.$cur_jv.'x_version_number'].'.min.js');
		
		$query = array(
			'UPDATE'	=> 'config',
			'SET'		=> 'conf_value=\''.$forum_db->escape($jquery_latest_version).'\'',
			'WHERE'		=> 'conf_name=\''.$forum_db->escape('o_pun_jquery_'.$cur_jv.'x_version_number').'\''
	    );
		$forum_db->query_build($query) or error(__FILE__, __LINE__);
		
		if (!defined('FORUM_CACHE_FUNCTIONS_LOADED'))
			require FORUM_ROOT.'include/cache.php';

		generate_config_cache();
	}
}
