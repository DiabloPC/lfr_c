<?php

if (!defined('FORUM'))
	die();

$lang_pun_jquery = array(
	'Setup jquery'						=> 'jQuery',
	'Setup jquery legend'				=> 'jQuery',
    'Version'					        => 'Версия jQuery',
	'Jquery version'				    => 'jQuery %s',
	'Include method'					=> 'Метод подключения jQuery',
	'Include method local label'		=> 'Локальный',
	'Include method google label'		=> 'Google Ajax API CDN ',
	'Include method microsoft label'	=> 'Microsoft CDN',
	'Include method jquery label'		=> 'jQuery CDN',
	'Outdated'		                    => '(устаревшая)',
	'Support older browsers'            => '(поддержка старых браузеров)',
	'New version is available'          => 'Доступна новая версия jQuery: %s',
	'Update'		                    => 'Обновить'
);

?>