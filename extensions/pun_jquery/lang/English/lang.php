<?php

if (!defined('FORUM'))
	die();

$lang_pun_jquery = array(
	'Setup jquery'						=> 'jQuery',
	'Setup jquery legend'				=> 'jQuery ',
	'Version'					        => 'Version jQuery',
	'Jquery version'				    => 'jQuery %s',
	'Include method'					=> 'Include method',
	'Include method local label'		=> 'Local',
	'Include method google label'		=> 'Google Ajax API CDN ',
	'Include method microsoft label'	=> 'Microsoft CDN',
	'Include method jquery label'		=> 'jQuery CDN',
	'Outdated'		                    => '(outdated)',
	'Support older browsers'            => '(support for older browsers)',
	'New version is available'          => 'A new version of jQuery is available: %s',
	'Update'		                    => 'Update'
);

?>
