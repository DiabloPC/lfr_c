<?php if (!defined('FORUM')) die();

if (isset($form['pun_jquery_include_method']))
{
	$form['pun_jquery_include_method'] = intval($form['pun_jquery_include_method'], 10);
	if (($form['pun_jquery_include_method'] < PUN_JQUERY_INCLUDE_METHOD_LOCAL) || ($form['pun_jquery_include_method'] > PUN_JQUERY_INCLUDE_METHOD_JQUERY_CDN))
	{
		$form['pun_jquery_include_method'] = PUN_JQUERY_INCLUDE_METHOD_LOCAL;
	}
}
else
{
	$form['pun_jquery_include_method'] = PUN_JQUERY_INCLUDE_METHOD_LOCAL;
}

