<?php
if (!defined('FORUM')) die();

    if (!isset($lang_pun_jquery)) {
		if (file_exists($ext_info['path'].'/lang/'.$forum_user['language'].'/lang.php')) {
			require $ext_info['path'].'/lang/'.$forum_user['language'].'/lang.php';
	    } else {
			require $ext_info['path'].'/lang/English/lang.php';
		}
	}
	$forum_loader->add_js($ext_info['url'] .'/js/update.js');

	if ($forum_config['o_pun_jquery_version'] == 0 || $forum_config['o_pun_jquery_version'] == 1)
	{
		if ($forum_config['o_pun_jquery_version'] == 0) { $cur_jv = 2; } else { $cur_jv = 1; }
		if ($jquery_latest_content = @file_get_contents('http://cdn.jsdelivr.net/jquery/'.$cur_jv.'/jquery.min.js')) {
            preg_match('/v(\d+\.\d+\.\d+)/', $jquery_latest_content, $matches);
            $jquery_latest_version = $matches[1];

            if (version_compare($jquery_latest_version, $forum_config['o_pun_jquery_'.$cur_jv.'x_version_number'], '>'))
            {              
			    $jquery_new_version = $matches[1];
            }
		}
	}

			// Reset counter
			$forum_page['group_count'] = $forum_page['item_count'] = 0;
?>
			<div class="content-head" id="<?php echo $ext_info['id'] ?>">
				<h2 class="hn"><span><?php echo $lang_pun_jquery['Setup jquery'] ?></span></h2>
			</div>

<?php	if (isset($jquery_new_version)) {  ?>
			    <div class="ct-box">
				    <p><?php echo sprintf($lang_pun_jquery['New version is available'], $jquery_new_version) ?></p>

					<span class="submit primary" style="float:right; margin-top:-27px;"><input type="submit" onClick="jQ.update_version(); return false;" value="<?php echo $lang_pun_jquery['Update'] ?>" /></span>
			    </div>
<?php	}  ?>

			<fieldset class="frm-group group<?php echo ++$forum_page['group_count'] ?>">
				<legend class="group-legend"><strong><?php echo $lang_pun_jquery['Setup jquery legend'] ?></strong></legend>
				
				<fieldset class="mf-set set<?php echo ++$forum_page['item_count'] ?>">
					<legend><span><?php echo $lang_pun_jquery['Version'] ?></span></legend>
					<div class="mf-box">
						<div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_version]" value="0"<?php if ($forum_config['o_pun_jquery_version'] == 0) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo sprintf($lang_pun_jquery['Jquery version'], $forum_config['o_pun_jquery_2x_version_number']) ?></label>
						</div>
						<div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_version]" value="1"<?php if ($forum_config['o_pun_jquery_version'] == 1) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo sprintf($lang_pun_jquery['Jquery version'], $forum_config['o_pun_jquery_1x_version_number'] .' '. $lang_pun_jquery['Support older browsers']) ?></label>
						</div>
						<div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_version]" value="2"<?php if ($forum_config['o_pun_jquery_version'] == 2) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo sprintf($lang_pun_jquery['Jquery version'], '1.7.1 ' .$lang_pun_jquery['Outdated']) ?></label>
						</div>
					</div>
				</fieldset>
				
				<fieldset class="mf-set set<?php echo ++$forum_page['item_count'] ?>">
					<legend><span><?php echo $lang_pun_jquery['Include method'] ?></span></legend>
					<div class="mf-box">
					    <div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_include_method]" value="<?php echo PUN_JQUERY_INCLUDE_METHOD_JQUERY_CDN; ?>"<?php if ($forum_config['o_pun_jquery_include_method'] == PUN_JQUERY_INCLUDE_METHOD_JQUERY_CDN) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo $lang_pun_jquery['Include method jquery label'] ?></label>
						</div>
						<div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_include_method]" value="<?php echo PUN_JQUERY_INCLUDE_METHOD_GOOGLE_CDN; ?>"<?php if ($forum_config['o_pun_jquery_include_method'] == PUN_JQUERY_INCLUDE_METHOD_GOOGLE_CDN) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo $lang_pun_jquery['Include method google label'] ?></label>
						</div>
						<div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_include_method]" value="<?php echo PUN_JQUERY_INCLUDE_METHOD_MICROSOFT_CDN; ?>"<?php if ($forum_config['o_pun_jquery_include_method'] == PUN_JQUERY_INCLUDE_METHOD_MICROSOFT_CDN) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo $lang_pun_jquery['Include method microsoft label'] ?></label>
						</div>
						<div class="mf-item">
							<span class="fld-input"><input type="radio" id="fld<?php echo ++$forum_page['fld_count'] ?>" name="form[pun_jquery_include_method]" value="<?php echo PUN_JQUERY_INCLUDE_METHOD_LOCAL; ?>"<?php if ($forum_config['o_pun_jquery_include_method'] == PUN_JQUERY_INCLUDE_METHOD_LOCAL) echo ' checked="checked"' ?> /></span>
							<label for="fld<?php echo $forum_page['fld_count'] ?>"><?php echo $lang_pun_jquery['Include method local label'] ?></label>
						</div>
					</div>
				</fieldset>
			</fieldset>

<?php


