<?php
if (!defined('FORUM')) die();

	if ($forum_config['o_pun_jquery_version'] == 2) {
		$j_version = '1.7.1';
	} else {
		if ($forum_config['o_pun_jquery_version'] == 0) { $cur_jv = 2; } elseif ($forum_config['o_pun_jquery_version'] == 1) { $cur_jv = 1; }
		$j_version = $forum_config['o_pun_jquery_'.$cur_jv.'x_version_number'];
	}

	switch ($forum_config['o_pun_jquery_include_method'])
    {
		case PUN_JQUERY_INCLUDE_METHOD_GOOGLE_CDN:
		    if ($data = @file_get_contents('http://ajax.googleapis.com/ajax/libs/jquery/'.$j_version.'/jquery.min.js')) {
			    $ext_pun_jquery_url = '//ajax.googleapis.com/ajax/libs/jquery/'.$j_version.'/jquery.min.js';
			} else {
				$ext_pun_jquery_url = $ext_info['url'].'/js/jquery-'.$j_version.'.min.js';
			}
			break;

		case PUN_JQUERY_INCLUDE_METHOD_MICROSOFT_CDN:
		    if ($data = @file_get_contents('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-'.$j_version.'.min.js')) {
			    $ext_pun_jquery_url = '//ajax.googleapis.com/ajax/libs/jquery/'.$j_version.'/jquery.min.js';
			} else {
				$ext_pun_jquery_url = $ext_info['url'].'/js/jquery-'.$j_version.'.min.js';
			}
			break;

		case PUN_JQUERY_INCLUDE_METHOD_LOCAL:
			$ext_pun_jquery_url = $ext_info['url'].'/js/jquery-'.$j_version.'.min.js';
			break;
			
		case PUN_JQUERY_INCLUDE_METHOD_JQUERY_CDN:
		    default:
			if ($data = @file_get_contents('http://code.jquery.com/jquery-'.$j_version.'.min.js')) {
			    $ext_pun_jquery_url = '//ajax.googleapis.com/ajax/libs/jquery/'.$j_version.'/jquery.min.js';
			} else {
				$ext_pun_jquery_url = $ext_info['url'].'/js/jquery-'.$j_version.'.min.js';
			}
			break;
	}

$forum_loader->add_js($ext_pun_jquery_url, array('type' => 'url', 'async' => false, 'group' => -100 , 'weight' => 75));


