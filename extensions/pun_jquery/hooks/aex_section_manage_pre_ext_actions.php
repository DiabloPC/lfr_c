<?php

if (!defined('FORUM')) die();

if ($ext['id'] == $ext_info['id'])
	$forum_page['ext_actions'][$ext_info['id']] = '<span><a href="'.forum_link($forum_url['admin_settings_features']).'#'.$ext_info['id'].'">'.$lang_admin_common['Settings'].'</a></span>';
