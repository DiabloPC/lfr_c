<?php if (!defined('FORUM')) die();

if ($forum_config['o_quickpost'] == '1' && !$forum_user['is_guest'] && ($cur_topic['post_replies'] == '1' || ($cur_topic['post_replies'] == '' && $forum_user['g_post_replies'] == '1'))
	&& ($cur_topic['closed'] == '0' || $forum_page['is_admmod']))
{
	if ($cur_post['poster_id'] != $forum_user['id']) {
		if ($cur_post['poster_id'] > 1)
			$forum_page['post_ident']['byline'] = '<span class="post-byline">'.sprintf((($cur_post['id'] == $cur_topic['first_post_id']) ? $lang_topic['Topic byline'] : $lang_topic['Reply byline']), (($forum_user['g_view_users'] == '1') ? '<a title="'.sprintf($lang_topic['Go to profile'], forum_htmlencode($cur_post['username'])).'" href="'.forum_link($forum_url['user'], $cur_post['poster_id']).'">'.forum_htmlencode($cur_post['username']).'</a>' : '<strong>'.forum_htmlencode($cur_post['username']).'</strong>')).'<a class="pan-nick-to-form" href="javascript://" id="paste_quote'.$cur_post['id'].'" title="'.$lang_pan_nick_to_form['title'].'" onClick="panNickToForm(\''.forum_htmlencode($cur_post['username']).'\');">⇓</a></span>';
		else
			$forum_page['post_ident']['byline'] = '<span class="post-byline">'.sprintf((($cur_post['id'] == $cur_topic['first_post_id']) ? $lang_topic['Topic byline'] : $lang_topic['Reply byline']), '<strong>'.forum_htmlencode($cur_post['username']).'</strong>').'<a class="pan-nick-to-form" href="javascript://" id="paste_quote'.$cur_post['id'].'" title="'.$lang_pan_nick_to_form['title'].'" onClick="panNickToForm(\''.forum_htmlencode($cur_post['username']).'\');">⇓</a></span>';
	}
}


