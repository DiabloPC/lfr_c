// PunBB 1.4 quick quote by Seiko
$(document).ready(function () {
    $("div.entry-content").mouseup(function (e) {
        username = $(this).parents("div.post:first").find(".post-byline").find("a").eq(0).text();

        var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || 'en').substr(0, 2).toLowerCase();
        if (lang == 'ru') {
            quotetext = 'Цитировать выделенное';
        }
        else {
            quotetext = 'Вставить как цитату';
        }

        if (window.getSelection) {
            theSelection = window.getSelection().toString();
        }
        else if (document.getSelection) {
            theSelection = document.getSelection();
        }
        else if (document.selection) {
            theSelection = document.selection.createRange().text;
        }

        $("#addQuote").remove();
        if (theSelection != false && e.which == 1 && username != false) {
            $("body").append('<div id="addQuote" style="left: ' + (e.pageX + 15) + 'px; top: ' + (e.pageY + 15) + 'px;">' + quotetext + '<div class="arrow-up"></div></div>');

            $("#addQuote").click(function () {
                $(this).animate({height:'0', opacity:'0'}, 350, function () {
                    $(this).remove();
                });

                PUNBB.pun_bbcode.insert_text('[quote="' + username + '"]' + theSelection + '[/quote]', '');
            });

            $("#addQuote").oneTime(2000, function () {
                if ($(this).is(':visible')) {
                    $(this).animate({height:'0', opacity:'0', left:'0'}, 350, function () {
                        $(this).remove();
                    });
                }
            });
        }
    });
});