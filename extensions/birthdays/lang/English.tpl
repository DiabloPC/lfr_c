Subject: Happy birthday

Hello <username>,

We at <board_title> would like to wish you a safe and happy <age> birthday.

--
<board_mailer>
(Do not reply to this message)