<?php
// Language definitions used in extensions
$lang_birthdays = array(
// index
'Birthdays today plural'	=>	'<strong>%s</strong> Birthdays today:',
'Birthdays today single'	=>	'<strong>%s</strong> Birthday today:',

'Birthdays soon plural'		=>	'<strong>%s</strong> Birthdays in the next %s days:',
'Birthdays soon single'		=>	'<strong>%s</strong> Birthday in the next %s days:',

// profile
'Birthday'					=>	'Birthday:',
'Age'						=>	'Age:',
'Month'						=>	'Month',
'Day'						=>	'Day',
'Year'						=>	'Year',
'Submit error'				=>	'The month and day of your birthday is required',

// month names
'Month names'				=> array(1 => // this makes sure that January is 1, February is 2 and so on
								'January',
								'February',
								'March',
								'April',
								'May',
								'June',
								'July',
								'August',
								'September',
								'October',
								'November',
								'December',
							),

// admin
'Features head'			=>	'Birhday Features',
'User birthdays'			=>	'User birthdays',
'User birthdays legend'		=>	'User birthdays settings:',
'Birthdays on index'		=>	'Birthdays on index:',
'Birthdays on index info'	=>	'Show birthdays happening today on the index page',
'Happy birthday email'		=>	'Happy birthday email:',
'Happy birthday email info'	=>	'Send out a email wishing users a happy birthday',
'Age limit'					=>	'Age limit:',
'Age limit info'			=>	'Set the limit of the year select in profile. (default is 13)',
'Upcoming range'			=>	'Upcoming Range:',
'Upcoming range info'		=>	'The number of days to show upcoming birthdays (default is 7. 0 = only today)',
);