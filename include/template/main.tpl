<!DOCTYPE html>
<html>
<head>
<!-- forum_head -->
</head>
<body>
	<!-- forum_messages -->
	<div id="brd-wrap" class="brd">
	<div <!-- forum_page -->>
	<div id="forum_logo_wrapper" class="forum_logo_wrapper">
		<div class="lfr-menu-button" id="lfr-menu-button">
		</div>
		<!-- forum_logo -->
		<div style="clear: both;">
		</div>
	</div>
		<nav class="lfr-mainmenu">
			<ul class="lfr-mainmenu-links">
				<!-- forum_navlinks -->
				<!-- forum_admod -->
			</ul>
			<!-- social_media -->
		</nav>
		<div class="lfr-usermenu">
			<!-- forum_welcome -->
			<p id="visit-links" class="options">
				<!-- forum_visit -->
			</p>
		</div>
	<!-- forum_announcement -->
	<div class="hr"><hr /></div>
	<div id="brd-main">
		<!-- forum_main_title -->
		<!-- forum_crumbs_top -->
		<!-- forum_main_menu -->
		<!-- forum_main_pagepost_top -->
		<!-- forum_main -->
		<!-- forum_main_pagepost_end -->
		<!-- forum_crumbs_end -->
	</div>
		<!-- forum_qpost -->
		<!-- forum_info -->
	<div class="hr"><hr /></div>
	<div id="brd-about">
		<!-- forum_about -->
	</div>
		<div class="scrolltop-wrap">
			<a href="#" role="button" aria-label="Scroll to top">
			<svg height="48" viewBox="0 0 48 48" width="48" height="48px" xmlns="http://www.w3.org/2000/svg">
				<path id="scrolltop-bg" d="M0 0h48v48h-48z"></path>
				<path id="scrolltop-arrow" d="M14.83 30.83l9.17-9.17 9.17 9.17 2.83-2.83-12-12-12 12z"></path>
			</svg>
			</a>
		</div>
		<!-- forum_debug -->
	</div>
	</div>
	<div id="tmla-touch-menu" class="touch-menu-la" oncontextmenu="return false">
		<div class="touch-menu-content">
			<div class="inner-header">
				<!-- forum_welcome -->
			</div>
			<ul class="menu-items">
				<!-- forum_navlinks -->
				<!-- forum_admod -->
			</ul>
			<div class="inner-footer">
				<!-- forum_visit -->
			</div>
		</div>
	</div>
	<script>
		var TouchMenu = TouchMenuLA({target: document.getElementById('tmla-touch-menu')});
		document.getElementById('lfr-menu-button').addEventListener('click', function(){TouchMenu.toggle();});
	</script>
	<!-- forum_javascript -->
	<script>
		var main_menu = responsiveNav("#brd-navlinks", {
		label: "<!-- forum_board_title -->"
		});
		if(document.getElementsByClassName('admin-menu').length){
		var admin_menu = responsiveNav(".admin-menu", {
			label: "<!-- forum_lang_menu_admin -->"
		});
		}
		if(document.getElementsByClassName('main-menu').length){
		var profile_menu = responsiveNav(".main-menu", {
			label: "<!-- forum_lang_menu_profile -->"
		});
		}
	</script>
	<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(52591894, "init", { id:52591894, clickmap:true, trackLinks:true, accurateTrackBounce:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/52591894" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</body>
</html>
