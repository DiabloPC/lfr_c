<!DOCTYPE html>
<html <!-- forum_local -->>
<head>
<!-- forum_head -->
</head>
<body>
<!-- forum_messages -->
<div id="brd-wrap" class="brd">
<div <!-- forum_page -->>

	<div id="forum_logo_wrapper" class="forum_logo_wrapper">
		<div class="lfr-menu-button" id="lfr-menu-button">
		</div>
		<!-- forum_logo -->
		<div style="clear: both;">
		</div>
	</div>

		<nav class="lfr-mainmenu">
			<ul class="lfr-mainmenu-links">
				<!-- forum_navlinks -->
				<!-- forum_admod -->
			</ul>
				<!-- social_media -->
		</nav>
		<div class="lfr-usermenu">
			<!-- forum_welcome -->
			<p id="visit-links" class="options">
				<!-- forum_visit -->
			</p>
		</div>

<div class="hr"><hr /></div>

<div id="brd-main">
	<!-- forum_main_title -->
	<!-- forum_crumbs_top -->
	<!-- forum_main_pagepost_top -->
	<!-- forum_admin_menu -->
	<!-- forum_admin_submenu -->
	<!-- forum_main -->
	<!-- forum_main_pagepost_end -->
	<!-- forum_crumbs_end -->
</div>

<div class="hr"><hr /></div>

<div id="brd-about">
	<!-- forum_about -->
</div>
<!-- forum_debug -->
</div>
</div>
	<div id="tmla-touch-menu" class="touch-menu-la" oncontextmenu="return false">
		<div class="touch-menu-content">
			<div class="inner-header">
				<!-- forum_welcome -->
			</div>
			<ul class="menu-items">
				<!-- forum_navlinks -->
				<!-- forum_admod -->
			</ul>
			<div class="inner-footer">
				<!-- forum_visit -->
			</div>
		</div>
	</div>
	<script>
		var TouchMenu = TouchMenuLA({target: document.getElementById('tmla-touch-menu')});
		document.getElementById('lfr-menu-button').addEventListener('click', function(){TouchMenu.toggle();});
	</script>
<!-- forum_javascript -->
<script>
    var main_menu = responsiveNav("#brd-navlinks", {
        label: "<!-- forum_board_title -->"
    });
    if(document.getElementsByClassName('admin-menu').length){
        var admin_menu = responsiveNav(".admin-menu", {
            label: "<!-- forum_lang_menu_admin -->"
        });
    }
    if(document.getElementsByClassName('main-menu').length){
        var profile_menu = responsiveNav(".main-menu", {
            label: "<!-- forum_lang_menu_profile -->"
        });
    }
</script>
</body>
</html>
